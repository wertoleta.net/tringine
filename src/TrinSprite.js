/**
 * Basic class to display animations.
 * Базовый класс для отображения анимаций.
 * @param {String} animationName Name of first animation. Имя начальной анимаций.
 * @returns {void}
 */
function TrinSprite(animationName) {
    TrinSprite.super.constructor.apply(this);
    this.currentAnimation = null;
    this.currentFrameNumber = 0;
    this.currentFrame = null;
    this.animationProgress = 0;
    this.animationSpeed = 30;
    this.looped = true;
    this.cutArea = {left: 0, top: 0, right: 0, bottom: 0};
    this.repeat = this.REPEAT_NO;
    this.abstract = false;
    this.repeatOrigin = {x: 0, y: 0};
    if (animationName !== undefined) {
        this.switchAnimation(animationName);
    }
}

extend(TrinSprite, TrinEntity);

/**
 * Repeat type constants.
 * Константы типа повторения.
 * @type Number
 */
TrinSprite.prototype.REPEAT_NO = 0;
TrinSprite.prototype.REPEAT_X = 1;
TrinSprite.prototype.REPEAT_Y = 2;

/**
 * Updates animations.
- * Обновляет анимацию.
 * @returns {void}
 */
TrinSprite.prototype.update = function() {
    TrinSprite.super.update.apply(this);
    if (this.currentAnimation !== null) {
        var prevFrame = this.currentFrameNumber;
        this.animationProgress += (this.animationSpeed === 0 ? 0 : 1 / (TrinGame.frameRate / this.animationSpeed));
        if (this.animationProgress >= this.currentAnimation.frames.length) {
            if (this.looped) {
                this.animationProgress -= Math.floor(this.animationProgress);
            } else {
                this.animationProgress = this.currentAnimation.frames.length - 1;
            }
        } else if (this.animationProgress < 0) {
            if (this.looped) {
                this.animationProgress += this.currentAnimation.frames.length;
            } else {
                this.animationProgress = 0;
            }
        }
        this.currentFrameNumber = Math.floor(this.animationProgress);
        if (this.currentFrameNumber !== prevFrame) {
            this.updateSize();
        }
    }
};

/**
 * Drawing instance animation frame to camera.
 * Рисует кадр из анимаций этой сущности на камеру.
 * @param {TrinCamera} camera Camera to draw in. Камера для отрисовки.
 * @returns {void}
 */
TrinSprite.prototype.draw = function(camera) {
    TrinSprite.super.draw.apply(this, [camera]);
    var animation = this.currentAnimation;
    if (animation !== null && this.visible && this.exists && (this.abstract || this.bounds.intersectsRect(camera))) {
        var context = camera.context;

        var frame = animation.frames[Math.floor(this.currentFrameNumber)];
        this.currentFrame = frame;

        var sx = frame.x + this.cutArea.left;
        var sy = frame.y + this.cutArea.top;
        var sw = frame.width - this.cutArea.right - this.cutArea.left;
        var sh = frame.height - this.cutArea.bottom - this.cutArea.top;
        var dx = this.x + this.cutArea.left - this.origin.x * this.scale.x - camera.x + frame.offset.x * this.scale.x;
        var dy = this.y + this.cutArea.top - this.origin.y * this.scale.y - camera.y + frame.offset.y * this.scale.y;
        var dw = sw * this.scale.x;
        var dh = sh * this.scale.y;

        if (this.angle !== 0) {
            dx -= this.x - camera.x;
            dy -= this.y - camera.y;
        }
        else
        {
            if (this.scale.x < 0) {
                context.translate(camera.width, 0);
                context.scale(-1, 1);
                context.translate(0, 0);
                dw = -dw;
                dx = (camera.width / 2 - (this.x - camera.width / 2)) - this.origin.x * this.scale.x - this.cutArea.left + camera.x + frame.offset.x * this.scale.x - dw;
            }
            if (this.scale.y < 0) {
                context.translate(0, camera.height);
                context.scale(1, -1);
                context.translate(0, 0);
                dh = -dh;
                dy = (camera.height / 2 - (this.y - camera.height / 2)) - this.origin.y * this.scale.y - this.cutArea.top + camera.y + frame.offset.y * this.scale.y - dh;
            }
        }
        if (sw > 0 && sh > 0 && dw !== 0 && dh !== 0) {
            switch (this.repeat) {
                case (this.REPEAT_NO):
                    {
                        context.drawImage(animation.image, sx, sy, sw, sh, dx, dy, dw, dh);
                    }
                    break;
                case (this.REPEAT_Y):
                    {
                        this.repeatOrigin.y = this.repeatOrigin.y % sh;
                        var repeatSize = this.repeatOrigin.y * this.scale.y;
                        if (repeatSize > 0) {
                            context.drawImage(animation.image, sx, sy + (sh - this.repeatOrigin.y),
                                    sw, this.repeatOrigin.y, dx, dy,
                                    dw, repeatSize);
                        }
                        if (dh - repeatSize > 0) {
                            context.drawImage(animation.image, sx, sy,
                                    sw, sh - this.repeatOrigin.y,
                                    dx, dy + this.repeatOrigin.y,
                                    dw, dh - repeatSize);
                        }
                    }
                    break;
                case (this.REPEAT_X):
                    {
						this.repeatOrigin.x = this.repeatOrigin.x % (sw + this.cutArea.right);
                        repeatSize = Math.min(this.repeatOrigin.x, sw) * this.scale.x;
						var overflow = Math.max(0, this.repeatOrigin.x - repeatSize / this.scale.x);
                        if (repeatSize > 0) {
                            context.drawImage(animation.image, sx + (sw - this.repeatOrigin.x + this.cutArea.right), sy,
                                    repeatSize, sh, dx, dy,
                                    repeatSize, dh);
                        }
                        if (dw - repeatSize > 0) {
                            context.drawImage(animation.image, sx, sy,
                                    sw - this.repeatOrigin.x, sh,
                                    dx + this.repeatOrigin.x, dy,
                                    dw - repeatSize, dh);
                        }
                    }
                    break;
            }
        }
    }

    if (TrinGame.debugger.debug) {
        this.drawBounds(camera);
    }
};

/**
 * Draw instance bounds.
 * Рисует границы сущьности.
 * @param {TrinCamera} camera Camera to draw in. Камера для отрисовки.
 * @returns {void}
 */
TrinSprite.prototype.drawBounds = function(camera) {
    var context = camera.context;
    context.restore();
    context.save();
    context.beginPath();
    context.strokeStyle = "#0000FF";
    context.arc(this.bounds.x - camera.x, this.bounds.y - camera.y, 2, 0, 2 * Math.PI, false);
    context.stroke();
    context.beginPath();
    context.strokeStyle = "#000000";
    context.globalAlpha = 1;
    context.rect(this.bounds.x - camera.x, this.bounds.y - camera.y, this.bounds.width, this.bounds.height);
    context.stroke();
    context.beginPath();
    context.strokeStyle = "#FF0000";
    context.arc(this.x - camera.x, this.y - camera.y, 5, 0, 2 * Math.PI, false);
    context.stroke();
};

/**
 * Switching beetwen animations.
 * Переключение между анимациями.
 * @param {String} animationName Name of animation to switch on. Имя анимаций на которую следует переключиться.
 * @returns {void}
 */
TrinSprite.prototype.switchAnimation = function(animationName) {
    var animation = TrinAnimation.prototype.getAnimation(animationName);
    if (animation === undefined || animation === null) {
        TrinGame.debugger.log("SpriteNoAnimation", [animationName]);
        return;
    }
    this.currentAnimation = animation;
    this.currentFrameNumber = this.animationProgress = 0;
    this.updateSize();
};

/**
 * Updates the instance bounds. 
 * Обновляет границы сущности.
 * @returns {void}
 */
TrinSprite.prototype.updateBounds = function() {
    if (this.currentFrame === null || this.currentFrame === undefined || this.currentFrame.offset === undefined) {
        TrinSprite.super.updateBounds.apply(this);
    } else {
        this.bounds.set(this.x - (this.origin.x + this.boundsOffset.left - this.currentFrame.offset.x) * this.scale.x * (this.scale.x > 0 ? 1 : (this.scale.x < 0 ? -1 : 0)),
                        this.y - (this.origin.y + this.boundsOffset.top - this.currentFrame.offset.y) * this.scale.y * (this.scale.y > 0 ? 1 : (this.scale.y < 0 ? -1 : 0)),
                (this.currentFrame.width - this.boundsOffset.right - this.boundsOffset.left - this.currentFrame.offset.x) * Math.abs(this.scale.x),
                (this.currentFrame.height - this.boundsOffset.bottom - this.boundsOffset.top - this.currentFrame.offset.y) * Math.abs(this.scale.y));
    }
};

/**
 * Updates the sprite size. 
 * Обновляет размер сущности.
 * @returns {void}
 */
TrinSprite.prototype.updateSize = function() {
	if (this.currentAnimation == null) {
		return;
	}
    var frame = this.currentAnimation.frames[Math.floor(this.currentFrameNumber)];
    this.width = frame.width * Math.abs(this.scale.x);
    this.height = frame.height * Math.abs(this.scale.y);
    this.updateBounds();
};

/**
 * Destroy and cleen the instance. 
 * Уничтожает и очищает сущьность
 * @returns {void}
 */
TrinSprite.prototype.destroy = function() {
    TrinSprite.super.destroy.apply(this);
    this.currentAnimation = null;
    this.cutArea = null;
    this.repeatOrigin = null;
};

/**
 * Test frame pixels in position near to point {x, y} for transparency.
 * !!!ATENTION!!! Works only with not transformed sprites.
 * Проверка пикселей кадра близких к позиций {x, y} на прозрачность.
 * !!!ВНИМАНИЕ!!!! Работает только с не трансформироваными спрайтами.
 * @param {Number} x X position of center pixel for test. Позиция центрального пикселя по оси Х.
 * @param {Number} y Y position of center pixel for test. Позиция центрального пикселя по оси Y.
 * @param {Integer} d Rectangle side size for test. Размер стороны квадрата для проверки.   
 * @param {TrinCamera} camera On which camera to test. На какой камере проверять.
 * @returns {Boolean} False if all pixel in rect is transparent. Возвращает False, если все пиксели оказались прозрачными.
 */
TrinSprite.prototype.hitTest = function(x, y, d, camera) {
    TrinSprite.super.hitTest.apply(this);
    var animation = this.currentAnimation;
    if (animation === null) {
        return false;
    }
    if (d === undefined || d < 1) {
        d = 1;
    }
    if (camera === undefined) {
        camera = TrinGame.camera;
    }
    d = Math.floor((d - 1) / 2);
    x -= this.x - camera.x;
    y -= this.y - camera.y;
    var frame = animation.frames[this.currentFrameNumber];

    var l = x - d;
    var r = x + d;
    var t = y - d;
    var b = y + d;

    if (l > this.width || r < 0 || t > this.height || b < 0) {
        return false;
    }

    var image = animation.image;
    var canvas = document.createElement("Canvas");
    canvas.width = frame.width;
    canvas.height = frame.height;
    var context = canvas.getContext("2d");
    context.drawImage(image, frame.x, frame.y, frame.width, frame.height, 0, 0, frame.width, frame.height);
    var imageData;
    var cx = 0;
    var cy = 0;
    try {
        imageData = context.getImageData(0, 0, frame.width, frame.height);
    } catch (e) {
        return true;
    }

    for (var i = -d; i <= d; i++) {
        for (var j = -d; j <= d; j++) {
            cx = x + j;
            cy = y + i;
            if (cx >= 0 && cx < this.width && cy >= 0 && cy < this.height) {
                if (imageData.data[((cy * frame.width + cx) * 4 + 3)] > 0) {
                    return true;
                }
            }
        }
    }
    return false;
};

/**
 * Returns true if animation is ended.
 * Возвращает true, если анимация закончилась.
 * @returns {Boolean}
 */
TrinSprite.prototype.isFinished = function() {
    return this.currentFrameNumber === this.currentAnimation.frames.length - 1;
};

/**
 * Start playeing animation from first frame.
 * Начинает проигрывать анимацию с начала.
 * @returns {void}
 */
TrinSprite.prototype.play = function() {
    this.currentFrameNumber = this.animationProgress = 0;
};
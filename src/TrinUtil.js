/**
 * Extends class. Contains a lots of *MAGIC*!
 * Наследует класс.
 * @param {Function} Child Child class. Класс наследник.
 * @param {Function} Parent Parent class. Класс родитель.
 * @returns {void}
 */
function extend(Child, Parent) {
    var F = function() {
    };
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.super = Parent.prototype;
}

function TrinUtil() {
}

/**
 * Device type constants
 * Константы типа устройства.
 * @type Number
 */
TrinUtil.prototype.DEVICE_PC            = 0;
TrinUtil.prototype.DEVICE_ANDROID       = 1;
TrinUtil.prototype.DEVICE_IOS           = 2;
TrinUtil.prototype.DEVICE_OTHER_MOBILE  = 3;

TrinUtil.prototype.BROWSER_OTHER = 0;
TrinUtil.prototype.BROWSER_SAFARI = 1;
TrinUtil.prototype.BROWSER_CHROME = 2;

/**
 * Just digits array.
 * Просто массив цифр.
 * @type String
 */
TrinUtil.prototype.digits = "0123456789ABCDEF";

/**
 * Global list of loaded json.
 * Глобальный список загруженный JSON файлов.
 * @type Array
 */
TrinUtil.prototype.LOADED_JSON = [];

TrinUtil.prototype.selectElement = null;
TrinUtil.prototype.selectCallback = null;
TrinUtil.prototype.selectCaller = null;

/**
 * Detects where game is runed.
 * Проверяет где запущена игра.
 * @returns {Boolean} True if game runed on mobile device. Истинно если игра запущена на мобильном устройстве.
 */
TrinUtil.prototype.isOtherMobile = function() {
    if (       navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i))
    {
        return true;
    } else {
        return false;
    }
};

/**
 * Check what game is runed on Android device.
 * Проверяет не запущена ли игра на Андроиде.
 * @returns {Boolean} True if game runed on Android device. Истинно, если игра запущена на Андроиде.
 */
TrinUtil.prototype.isAndroid = function(){
    return navigator.userAgent.match(/Android/i);
};


/**
 * Check what game is runed on IDevice (IPad, IPhone etc.).
 * Проверяет не запущена ли игра на айУстройстве (айПод, айФон и т.д.).
 * @returns {Boolean}
 */
TrinUtil.prototype.isIOSDevice = function(){
    return (navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i));
};

/**
 * Converts number to dec format.
 * Конвертирует число в десятичное представление.
 * @param {String} number Number to convert. Число для конвертаций.
 * @param {Integer} base Notation base of number. В какой системе счисления число.
 * @returns {Number} Converted number. Конвертированое число.
 */
TrinUtil.prototype.numberToDec = function(number, base) {
    var dec = 0;
    var p = 1;
    number = number.toUpperCase();
    for (var i = number.length - 1; i >= 0; i--) {
        var c = number[i];
        dec += this.digits.indexOf(c) * p;
        p *= base;
    }
    return dec;
};

/**
 * Converts number in dec to number with other notation base.
 * Конвертирует число из десятичной системы счисления в другую. 
 * @param {Integer} dec Number in dec. Число в десятичной системе счисления.
 * @param {Integer} base New notation base. Новое основание системы счисления.
 * @param {Integer} len Min number length. Минимальная длина числа.
 * @returns {String}
 */
TrinUtil.prototype.decToNumber = function(dec, base, len) {
    if (len === undefined) {
        len = 2;
    }
    var number = "";
    while (dec > 0) {
        number = this.digits[dec % base] + number;
        dec = Math.floor(dec / base);
    }
    while (number.length < len) {
        number = "0" + number;
    }
    return number;
};

/**
 * Returns type of player device.
 * Возвращает тип устройства игрока.
 * @returns {Integer|TrinUtil.DEVICE_PC|TrinUtil.DEVICE_ANDROID|TrinUtil.DEVICE_IOS|TrinUtil.DEVICE_OTHER_MOBILE}
 */
TrinUtil.prototype.getDeviceType = function()   {
    if (this.isAndroid())   {
        return this.DEVICE_ANDROID;
    }   
    else if (this.isIOSDevice())    {
        return this.DEVICE_IOS;
    }   
    else if (this.isOtherMobile())  {
        return this.DEVICE_OTHER_MOBILE;
    }   
    return this.DEVICE_PC;
};

TrinUtil.prototype.isSafari = function(){
	if (TrinUtil.prototype.isChrome())	{
		return false;
	}
    return navigator.userAgent.match(/Safari/i);
};

/**
 * Check what game is runed in Chrome browser.
 * Проверяет не запущена ли игра в Хроме.
 * @returns {Boolean} True if game runed in Chrome browser. Истинно, если игра запущена в Хроме.
 */
TrinUtil.prototype.isChrome = function(){
    return navigator.userAgent.match(/Chrome/i);
};

TrinUtil.prototype.isMacintosh = function(){
    return navigator.userAgent.match(/Macintosh/i);
};

TrinUtil.prototype.getBrowserType = function()   {
    if (this.isChrome())    {
        var navU = navigator.userAgent;

        // Android Mobile
        var isAndroidMobile = navU.indexOf('Android') > -1 && navU.indexOf('Mozilla/5.0') > -1 && navU.indexOf('AppleWebKit') > -1;

        // Apple webkit
        var regExAppleWebKit = new RegExp(/AppleWebKit\/([\d.]+)/);
        var resultAppleWebKitRegEx = regExAppleWebKit.exec(navU);
        var appleWebKitVersion = (resultAppleWebKitRegEx === null ? null : parseFloat(regExAppleWebKit.exec(navU)[1]));

        // Chrome
        var regExChrome = new RegExp(/Chrome\/([\d.]+)/);
        var resultChromeRegEx = regExChrome.exec(navU);
        var chromeVersion = (resultChromeRegEx === null ? null : parseFloat(regExChrome.exec(navU)[1]));

        // Native Android Browser
        var isAndroidBrowser = isAndroidMobile && (appleWebKitVersion !== null && appleWebKitVersion < 537) || (chromeVersion !== null && chromeVersion < 37)
        if (isAndroidBrowser)   {
            return this.BROWSER_OTHER;   
        }
        return this.BROWSER_CHROME;
    }else if (this.isSafari())   {
        return this.BROWSER_SAFARI;
    }     
    return this.BROWSER_OTHER;
};

/**
 * Initialize main HTML elements.
 * Инициализирует базовые HTML элементы.
 * @returns {void}
 */
TrinUtil.prototype.initIndex = function()   {
    var html = document.getElementsByTagName('html')[0];
    var body = document.getElementsByTagName('body')[0];
    html.style.overflow = body.style.overflow = "hidden";
    
    var div = document.createElement("div");
    div.id = "TrinGameContainer";
    div.style.position = "absolute";
    div.style.left = "50%";
    div.style.top = "50%";
    div.style.zIndex = "-1000";
    document.getElementsByTagName('body')[0].appendChild(div);
    
    
    var canvas = document.createElement("Canvas");
    canvas.id = "TrinGameCanvas";
    canvas.style.width = "100%";
    canvas.style.height = "100%";
    div.appendChild(canvas);
	
	var selectElement = document.createElement("select");
	selectElement.style.position = "absolute";
	selectElement.style.left = "0";
	selectElement.style.top = "0";
	selectElement.style.display = "none";
    div.appendChild(selectElement);
	this.selectElement = selectElement;
	this.selectElement.onclick = this.onSelectElementChange;
	this.selectElement.onblur = this.hideSelectElement;
	this.selectElement.onchange = this.onSelectElementChange;
};

/**
 * Sets HTML background.
 * Устанавливает задний фон на уровне HTML.
 * @param {String|"#RRGGBB"} color Color of background. Цвет заднего фона.
 * @param {String|Url} url URL to background image. Адрес изображения которое следует использовать для заднего фона.
 * @returns {void}
 */
TrinUtil.prototype.setGlobalBackground= function(color, url){
    var body = document.getElementsByTagName("body")[0];
    body.style.backgroundColor = color;
    if (url !== undefined)  {
        body.style.backgroundImage = "url(\""+url+"\")";
    }
};

TrinUtil.prototype.getDistance = function(x1, y1, x2, y2, k)   {
    if (k === undefined)    {
        k = 1;
    }
    var dx=(x1 - x2)*(x1 - x2);
    var dy = (y1 - y2) * k;
    dy *=dy;
    return Math.sqrt(dx+dy);
};

TrinUtil.prototype.stacktrace = function(max) {
  var i = 0;
  if (max === undefined)    {
      max = 20;
  }
  function st2(f) {
      i++;
      if (i > max)  {
          return "";
      }
    return !f ? "" : st2(f.caller) + "\n----\n" + f.toString(); 
        //st2(f.caller).concat([f.toString().split('(')[0].substring(9) + '(' + f.arguments.join(',') + ')']);
  }
  return st2(arguments.callee.caller);
};

TrinUtil.prototype.showComboboxSelect = function(items, selectedIndex, caller, onSelect){
	this.selectCallback = onSelect;
	this.selectCaller = caller;
	while (this.selectElement.options.length > 0) {
		this.selectElement.remove(0);
	}
	for (var i = 0; i < items.length; i++){
		var opt = document.createElement('option');
		opt.value = i;
		opt.innerHTML = items[i];
		if (i == selectedIndex) {
			opt.selected = true;
		}
		this.selectElement.appendChild(opt);
	}
    this.selectElement.style.display = "block";
	this.selectElement.style.left = (TrinGame.mouse.x * TrinGame.scaleFactor) + "px";
	this.selectElement.style.top = (TrinGame.mouse.y * TrinGame.scaleFactor) + "px";
    setTimeout(function() {
		var event;
		event = document.createEvent('MouseEvents');
		event.initMouseEvent('mousedown', true, true, window);
		TrinUtil.prototype.selectElement.dispatchEvent(event);
    }, 10);
} 

TrinUtil.prototype.selectItemSelected = function(){
	console.log(this);
}

TrinUtil.prototype.hideSelectElement = function(){
	TrinUtil.prototype.selectElement.style.display = "none";
}

TrinUtil.prototype.onSelectElementChange = function(){
	TrinUtil.prototype.hideSelectElement();
	TrinUtil.prototype.selectCallback.apply(TrinUtil.prototype.selectCaller, [TrinUtil.prototype.selectElement.value]);
}
/**
 * Class for separating drawing queue.
 * Класс для разделения очереди рисования.
 * @returns {void}
 */
function TrinLayer() {
    TrinLayer.super.constructor.apply(this);
    this.autoReviveChildren = false;
    this.children = null;
    this.numChildren = 0;
    this.sortIndex = "";
    this.sortOrder = this.ASCEDING;
    this.supportLayer = false;
}

extend(TrinLayer, TrinBasic);

/**
 * Sort order constants.
 * Константи порядка сортировки.
 * @type Number
 */
TrinLayer.prototype.ASCEDNING = -1;
TrinLayer.prototype.DESCENDING = 1;

/**
 * Destroy and cleen the instance and it childrens. 
 * Уничтожает и очищает сущьность и её детей.
 * @returns {void}
 */
TrinLayer.prototype.destroy = function() {
    TrinLayer.super.destroy.apply(this);
    if (this.children !== null && !this.supportLayer) {
        var entity;
        var i = 0;
        while (i < this.numChildren) {
            entity = this.children[i++];
            if (entity !== null) {
                entity.destroy();
            }
        }

        this.children = null;
    }
};

/**
 * Kills the instance (Not destroy) and it childrens. 
 * Убивает сущьность (Не уничтожает) и всех её детей.
 * @returns {void}
 */
TrinLayer.prototype.kill = function() {
    TrinLayer.super.kill.apply(this);
    if (this.children !== null && !this.supportLayer) {
        var entity;
        var i = 0;
        while (i < this.numChildren) {
            entity = this.children[i++];
            if (entity !== null && entity.exists) {
                entity.kill();
            }
        }
    }
};

/**
 * Revive the instance and it childrens (if reviveChildren flag is true).
 * Воскрещает сущьность и её детей (Если reviveChildren равно true).
 * @returns {void}
 */
TrinLayer.prototype.revive = function() {
    TrinLayer.super.revive.apply(this);

    if (this.autoReviveChildren && this.children !== null && !this.supportLayer) {
        var entity;
        var i = 0;
        while (i < this.numChildren) {
            entity = this.children[i++];
            if (entity !== null) {
                entity.revive();
            }
        }
    }
};

/**
 * Update instance and all instance childrens.
 * Обновляет сущность и всех её детей.
 * @returns {void}
 */
TrinLayer.prototype.update = function() {
    TrinLayer.super.update.apply(this);
    if (this.children !== null && !this.supportLayer) {
        var entity;
        var i = 0;
        while (i < this.numChildren) {
            entity = this.children[i++];
            if (entity !== null && entity.exists && entity.active) {
                entity.update();
                entity.postUpdate();
            }
        }
    }
};

/**
 * Draw instance and all it childrens.
 * Рисует сущность и всех её детей.
 * @param {TrinCamera} camera
 * @returns {void}
 */
TrinLayer.prototype.draw = function(camera) {
    TrinLayer.super.draw.apply(this, [camera]);
    if (this.children !== null) {
        var entity;
        var i = 0;
        while (i < this.numChildren) {
            entity = this.children[i++];
            if (entity !== null && entity.exists && entity.visible) {
                entity.draw(camera);
            }
        }
    }
};

/**
 * Sort instance childrens.
 * Сортирует детей сущности.
 * @param {String} fieldName Childrens parameter name to sort by. Название параметра по которому сортировать детей.
 * @param {Sort order constant} order Order to sort by. В каком порядке сортировать.
 * @returns {void}
 */
TrinLayer.prototype.sort = function(fieldName, order) {
    if (fieldName === undefined) {
        fieldName = "y";
    }
    if (order === undefined) {
        order = this.DESCENDING;
    }
    if (this.children !== null) {
        this.sortIndex = fieldName;
        this.sortOrder = order;
        this.children.sort(this.sortHandler);
    }
};

/**
 * Sort handler.  
 * Обработчик сортировкию
 * @param {TrinBasic} entity1
 * @param {TrinBasoc} entity2
 * @returns {Number}
 */
TrinLayer.prototype.sortHandler = function(entity1, entity2) {
    var sortIndex = "y";
    var sortOrder = -1;
    if (entity1 === null) {
        return sortOrder;
    } else if (entity2 === null) {
        return -sortOrder;
    }

    if (entity1[sortIndex] < entity2[sortIndex]) {
        return sortOrder;
    } else if (entity1[sortIndex] > entity2[sortIndex]) {
        return -sortOrder;
    }
    return 0;
};

/**
 * Add instance to layer.
 * Добавляет сущность в слой.
 * @param {TrinBasic} entity Entity to add. Сущность для добавления.
 * @returns {void}
 */
TrinLayer.prototype.add = function(entity) {
    if (this.children === null) {
        this.children = [];
    }

    if (this.children.indexOf(entity) > -1) {
        return entity;
    }

    if (entity.parent !== null && !this.supportLayer) {
        entity.parent.remove(entity);
    }

    if (!this.supportLayer) {
        entity.parent = this;
    }

    var i = 0;
    var n = this.children.length;
    while (i < n) {
        if (this.children[i] === null) {
            this.children[i] = entity;
            return entity;
        }
        i++;
    }

    this.children[n] = entity;
    this.numChildren++;
    return entity;
};

/**
 * Removes instance from layer.
 * Убирает сущность из слоя.
 * @param {TrinBasic} entity Entinity to remove. Сущность для изъятия.
 * @param {Boolean} splice Split array or not. Если false - на месте сущности останеся null.
 * @returns {void}
 */
TrinLayer.prototype.remove = function(entity, splice) {
    if (this.children === null) {
        return entity;
    }

    var i = this.children.indexOf(entity);
    if (i < 0 || i >= this.children.length) {
        return entity;
    }

    this.children[i] = null;
    if (!this.supportLayer) {
        entity.parent = null;
    }

    if (splice) {
        this.children.splice(i, 1);
        this.numChildren--;
    }

    return entity;
};

/**
 * Check layer to contain the entity.
 * Проверяет, содержит ли слой сущность.
 * @param {TrinBasic} entity Entity to serach. Сущность для поиска. 
 * @returns {Boolean}
 */
TrinLayer.prototype.contains = function(entity) {
    if (this.children === null) {
        return false;
    }
    return (this.children.indexOf(entity) >= 0) ? true : false;
};

/**
 * Removes first killed instance of class.
 * Возвращает первую убитую сущность класса.
 * @param {Class} _class Class for recycling. Класс для переработки.
 * @returns {TrinBasic}
 */
TrinLayer.prototype.recycle = function(_class) {
    var entity = this.getAvailable(_class);
    if (entity !== null) {
        return entity;
    }

    if (_class === undefined || _class === null) {
        return null;
    }

    entity = new _class();
    return (entity instanceof TrinBasic) ? this.add(entity) : null;
};

TrinLayer.prototype.getAvailable = function(_class) {
    if (this.children === null) {
        return null;
    }
    var entity;
    var i = 0;
    while (i < this.numChildren) {
        entity = this.children[i++];
        if (entity !== null && !entity.exists && ((_class === undefined) || (entity instanceof _class))) {
            return entity;
        }
    }
    return null;
};

/**
 * Replace on entity by other. Returns old entity.
 * Заменяет одну сущность другой. Возвращает заменяемую сущность.
 * @param {TrinBasic} oldEntity Entity to replace. Заменяемая сущность.
 * @param {TrinBasic} newEntity Entity to add. Сущьность которой заменяем.
 * @returns {TrinBasic} Replaceble entity. Заменяемая сущность.
 */
TrinLayer.prototype.replace = function(oldEntity, newEntity) {
    if (this.children === null) {
        return newEntity;
    }

    var i = this.children.indexOf(oldEntity);
    if (i >= 0 && i < this.children.length) {
        if (newEntity.parent !== null && newEntity.parent !== this && !this.supportLayer) {
            newEntity.parent.remove(newEntity);
            newEntity.parent = this;
        }

        this.children[i] = newEntity;
        if (!this.supportLayer) {
            oldEntity.parent = null;
        }
    }

    return oldEntity;
};

/**
 * Swap two entities.
 * Меняет местами две сущности.
 * @param {TrinBasic} entityA First swaping entity. Первая сущность для обмена.
 * @param {TrinBasic} entityB Second swaping entity. Вторая сущность для обмена.
 * @returns {void}
 */
TrinLayer.prototype.swap = function(entityA, entityB) {
    if (this.children === null) {
        return;
    }

    var iA = this.children.indexOf(entityA);
    var iB = this.children.indexOf(entityB);
    if (iA >= 0 && iA < this.children.length && iB >= 0 && iB < this.children.length)
    {
        this.children[iA] = entityB;
        this.children[iB] = entityA;
    }
};

/**
 * Removes all childs from layer.
 * Убирает все сущности из слоя.
 * @param {Boolean} destroy Destroy flag. If true - all childres will be destroyed. Если true - все сущности будут уничтожены.
 * @returns {void}
 */
TrinLayer.prototype.removeAll = function(destroy) {
    if (destroy === undefined) {
        destroy = false;
    }
    if (this.children === null) {
        return;
    }

    var entity;
    var i = 0;
    while (i < this.numChildren) {
        entity = this.children[i++];
        if (entity !== null) {
            if (destroy) {
                entity.destroy();
            }
            if (!this.supportLayer) {
                entity.parent = null;
            }
            this.children[i] = null;
        }
    }

    this.numChildren = 0;
};

/**
 * Returns first alive entity.
 * Возвращает первую живую сущность.
 * @param {Class} _class Class of searching entity. Класс искомой сущности.
 * @returns {TrinBasic} Firt alive entity. Первая живая сущность.
 */
TrinLayer.prototype.getAlive = function(_class) {
    if (this.children === null) {
        return null;
    }

    var entity;
    var i = 0;
    while (i < this.numChildren) {
        entity = this.children[i++];
        if (entity !== null && entity.exists && entity.alive &&
                ((_class === null) || (entity instanceof _class))) {
            return entity;
        }
    }

    return null;
};


/**
 * Returns first killed entity.
 * Возвращает первую убитую сущность.
 * @param {Class} _class Class of searching entity. Класс искомой сущности.
 * @returns {TrinBasic} Firt killed entity. Первая убитая сущность.
 */
TrinLayer.prototype.getDead = function(_class) {
    if (this.children === null) {
        return null;
    }

    var entity;
    var i = 0;
    while (i < this.numChildren) {
        entity = this.children[i++];
        if (entity !== null && !entity.alive &&
                ((_class === null) || (entity instanceof _class))) {
            return entity;
        }
    }

    return null;
};

/**
 * Set field of all childrens to one value.
 * Устанавливает поле всех детей на заданную величину.
 * @param {String} fieldName Name of setted field. Имя определяемого поля.
 * @param {Object} value New value of field. Новое значение поля.
 * @param {Boolean} recurse Call same function for childrens? Вызывать эту функцию для всех детей?
 * @returns {void}
 */
TrinLayer.prototype.setAll = function(fieldName, value, recurse) {
    var entity;
    var i = 0;
    while (i < this.numChildren) {
        entity = this.children[i++];
        if (entity !== null) {
            if (recurse && entity instanceof TrinLayer) {
                entity.setAll(fieldName, value, recurse);
            } else {
                entity[fieldName] = value;
            }
        }
    }
};

/**
 * Call function on all childrens.
 * Вызывает функцию у всех детей.
 * @param {Function} f Function to call. Функция для вызова.
 * @param {Array} args Function arguments. Аргументы функций.
 * @param {Boolean} recurse Call same function on child childrens? Вызывать ли эту функцию у детей? 
 * @returns {void}
 */
TrinLayer.prototype.callAll = function(f, args, recurse) {
    var entity;
    var i = 0;
    while (i < this.numChildren) {
        entity = this.children[i++];
        if (entity !== null) {
            if (recurse && entity instanceof TrinLayer) {
                entity.callAll(f, args, recurse);
            } 
            f.apply(entity, args);
        }
    }
};
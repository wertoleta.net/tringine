/**
 * Button calss.
 * Класс кнопки.
 * @param {String} animationName Name of button animation (1-3 frames). Имя анимаций кнопки (1-3 кадра). 
 * @param {String} text Text to draw on button. Текст для рисования на кнопке.
 * @param {Function} onClick On click callback. Функция выполняемая при клике.
 * @param {type} opensLink Set this flag true if button open link (Prevents pop-up blocking). Открывает ли функция окно (Нужно для предотвращения блокировки как поп-ап окна).
 * @returns {void}
 */
function TrinButton(animationName, text, onClick, opensLink) {
    TrinButton.super.constructor.apply(this);
    if (opensLink === undefined) {
        opensLink = false;
    }
    this.onClick = onClick;
    if (animationName !== null) {
        this.switchAnimation(animationName);
    }
    this.animationSpeed = 0;
    this.opensLink = opensLink;
    this.hovered = false;
    this.label = null;
    this.drawBack = true;
    this.scaleFactor = {x: 1, y: 1};
    if (this.currentAnimation === null) {
        this.width = 150;
        this.height = 50;
        this.center();
        this.updateBounds();
    }
    if (text !== undefined && text.length > 0)
    {
        this.label = new TrinText(text);
        this.label.setStyle("Arial", 32, true, "#FFFFFF", "center", "middle");
        this.label.reset(this.width / 2, this.height / 2);
    }
    if (this.opensLink) {
        this.onCanvasClick = function() {
            var button = arguments.callee.button;
            if (button.onClick !== undefined && button.onClick !== null && button.active &&
                    button.bounds.intersects(TrinGame.mouse.x + TrinGame.camera.x, TrinGame.mouse.y + TrinGame.camera.y)) {
                button.onClick.apply(this);
            }
        };
        this.onCanvasClick.button = this;
        var isXdk;
        try {
            isXdk = intel === undefined || !intel.isxdk;
        }catch(e)   {
            isXdk = false;   
        }
        if (TrinGame.isMobile && !isXdk) {
            TrinGame.canvas.addEventListener(this.touchEventName, this.onCanvasClick, false);
        } else {
            TrinGame.canvas.addEventListener(this.clickEventName, this.onCanvasClick, false);
        }
    }
}

extend(TrinButton, TrinSprite);

/**
 * Event types constants.
 * Константы типов событий.
 * @type String
 */
TrinButton.prototype.clickEventName = "click";
TrinButton.prototype.touchEventName = "touchend";
TrinButton.prototype.oneButtonAlreadyHovered = false;
TrinButton.prototype.data = null;
TrinButton.prototype.labelImagePatterns = {basic: "", hovered: ""};
TrinButton.prototype.labelColors = {basic: "white", hovered: "white"};
TrinButton.prototype.buttonColors = {basic: {stroke: "#103760", fill: "#17508F"}, hovered: {stroke: "#103760", fill: "#17508F"}};
TrinButton.prototype.animationState = -1;
TrinButton.prototype.animationProcess = 0;
TrinButton.prototype.animationDelay = 0;
TrinButton.prototype.animationScale = {x: 0, y: 0};
TrinButton.prototype.minAnimationSpeed = 0.04;
TrinButton.prototype.animationSpeedDivider = 8;
TrinButton.prototype.maxAnimationSize = 1.25;
TrinButton.prototype.hoverAnimationSize = 1.125;
TrinButton.prototype.oneStepAnimation = true;

/**
 * Update button state.
 * Обновляет состояние кнопки.
 * @returns {void}
 */
TrinButton.prototype.update = function() {
    TrinButton.super.update.apply(this);
    if (this.active) {
        this.scale.x = this.scale.y = 1;
        if (!this.oneButtonAlreadyHovered && this.bounds.intersects(TrinGame.mouse.x, TrinGame.mouse.y)) {
            TrinButton.prototype.oneButtonAlreadyHovered = true;
            this.hovered = true;
			if (this.animationState === -1 || this.animationState === 4) {
				this.animationState = 3;
				if (this.animationState === 4) {
					this.animationProcess = 1 - this.animationProcess;
				}else{
					this.animationProcess = 0;
				}
			}
            if (this.currentAnimation !== null && this.currentAnimation.frames.length > 1) {
                this.currentFrameNumber = 1;
            }   else {
                this.scale.x = 1.1 * this.scaleFactor.x;
                this.scale.y = 1.1 * this.scaleFactor.y;
            }
            if (TrinGame.mouse.isReleased()) {
                if (this.currentAnimation !== null) {
                    if (this.currentAnimation.frames.length > 2) {
                        this.currentFrameNumber = 2;
                    } else {
                        this.currentFrameNumber = 0;
                    }
                    this.scale.x = 1 * this.scaleFactor.x;
                    this.scale.y = 1 * this.scaleFactor.y;
                }
                if (!this.opensLink && this.onClick !== undefined && this.onClick !== null) {
                    this.onClick.apply(this);
                }
            }
            if (this.label !== null)
            {
				this.label.color = this.labelColors.hovered;
                this.label.patternImage = this.labelImagePatterns.hovered;
            }
        } else {
            this.currentFrameNumber = 0;
            this.hovered = false;
            this.scale.x = 1 * this.scaleFactor.x;
            this.scale.y = 1 * this.scaleFactor.y;
            if (this.label !== null)
            {
                this.label.color = this.labelColors.basic;
                this.label.patternImage = this.labelImagePatterns.basic;
            }
			if (this.animationState === 3) {
				this.animationState = 4;
				this.animationProcess = 1 - this.animationProcess;
			}
        }
		
		if (this.animationState > -1) {
			if (this.animationDelay > 0) {
				this.animationDelay--;
			}
			if (this.animationDelay <= 0) {
				this.animationProcess = Math.min(1, this.animationProcess + Math.max(this.minAnimationSpeed, (1 - this.animationProcess) / this.animationSpeedDivider));
				switch (this.animationState) {
					case 0:
						this.animationScale.x = this.animationScale.y = this.animationProcess * (this.maxAnimationSize);
						break;
					case 1:
						this.animationScale.x = this.animationScale.y = this.maxAnimationSize - (this.maxAnimationSize - 1) * this.animationProcess;
						break;
					case 2:
						this.animationState = -1;
						this.oneStepAnimation = true;
						break;
					case 3:
						this.animationScale.x = this.animationScale.y = 1 + this.animationProcess * (this.hoverAnimationSize - 1);					
						break;
					case 4: 
						this.animationScale.x = this.animationScale.y = this.hoverAnimationSize - (this.hoverAnimationSize - 1) * this.animationProcess;
						if (this.animationProcess === 1) {
							this.animationState = -1;
						}
						break;
					case 100:
						this.animationScale.x = this.animationScale.y = 1 + this.animationProcess * (this.maxAnimationSize - 1);
						break;
					case 101:
						this.animationScale.x = this.animationScale.y = (1 - this.animationProcess) * (this.maxAnimationSize);
						break;
					case 102:
						this.animationState = -1;
						this.oneStepAnimation = true;
						this.active = false;
						break;
				}
				if (this.animationProcess === 1 && !this.oneStepAnimation) {
					this.animationProcess = 0;
					this.animationState++;
				}
			}
			this.scale.x *= this.animationScale.x;
			this.scale.y *= this.animationScale.y;
		}
    }
	this.updateSize();
	if (this.label !== null) {
		var tw = this.width;
		var th = this.height;
		if (this.currentAnimation === null) {
			tw *= this.scale.x;
			th *= this.scale.y;
		}
		this.label.scale.x = this.scale.x;
		this.label.scale.y = this.scale.y;
		this.label.reset(tw / 2, th / 2);
	}
};

TrinButton.prototype.reset = function(x, y) {
	TrinButton.super.reset.apply(this, [x, y]);
	if (this.label != null) {
		var tw = this.width;
		var th = this.height;
		if (this.currentAnimation === null) {
			tw *= this.scale.x;
			th *= this.scale.y;
		}
		this.label.reset(tw / 2, th / 2);
	}	
}

TrinButton.prototype.appear = function(delay){
	if (delay === undefined) {
		delay = 0;
	}
	this.animationState = 0;
	this.animationProcess = 0;
	this.animationScale = {x: 0, y: 0};
	this.animationDelay = delay;
	this.oneStepAnimation = false;
}

TrinButton.prototype.disappear = function(delay){
	if (delay === undefined) {
		delay = 0;
	}
	this.animationState = 100;
	this.animationProcess = 0;
	this.animationScale = {x: 1, y: 1};
	this.animationDelay = delay;
	this.oneStepAnimation = false;
}

/**
 * Destroy and cleen the instance. 
 * Уничтожает и очищает сущьность
 * @returns {void}
 */
TrinButton.prototype.destroy = function() {
    TrinButton.super.destroy.apply(this);
    if (this.opensLink) {
        if (TrinGame.isMobile) {
            TrinGame.canvas.removeEventListener(this.touchEventName, this.onCanvasClick, false);
        } else {
            TrinGame.canvas.removeEventListener(this.clickEventName, this.onCanvasClick, false);
        }
    }
};

/**
 * Draws button and text on it.
 * Рисует кнопку и текст на ней.
 * @param {TrinCamera} camera
 * @returns {void}
 */
TrinButton.prototype.draw = function(camera) {
    TrinButton.super.draw.apply(this, [camera]);
    if (this.currentAnimation === null && this.drawBack) {
        var context = camera.context;
        context.beginPath();
        context.strokeStyle = (this.hovered ? this.buttonColors.hovered.stroke : this.buttonColors.basic.stroke);
        context.fillStyle = (this.hovered ? this.buttonColors.hovered.fill : this.buttonColors.basic.fill);
        context.rect(this.bounds.x - camera.x, this.bounds.y - camera.y, this.bounds.width, this.bounds.height);
        context.fill();
        context.stroke();
    }
    if (this.label !== null) {
		var px = this.label.x;
		var py = this.label.y;
		this.updateBounds();
        this.label.reset(this.bounds.x + this.label.x, this.bounds.y + this.label.y);
        this.label.draw(camera);
        this.label.reset(px, py);
    }
    if (TrinGame.debugger.debug) {
        this.drawBounds(camera);
    }
};
/**
 * Basic game state entity.
 * Базовая сущность состояния игры.
 * @returns {void}
 */
function TrinState() {
    this.group = new TrinLayer();
}

/**
 * Calls once before first update call. Use it for state initialization.
 * Вызывается один раз перед первым updste. Используйте для инициализаций.
 * @returns {undefined}
 */
TrinState.prototype.create = function() {
};

/**
 * Updates game state main group.
 * Обновляет главную группу состояния.
 * @returns {void}
 */
TrinState.prototype.update = function() {
    this.group.update();
};

/**
 * Draws game state main group.
 * Отрисовывает главную группу состояния.
 * @param {TrinCamera} camera Camera for drawing. Камера на которой рисовать.
 * @returns {void}
 */
TrinState.prototype.draw = function(context) {
    this.group.draw(context);
};

/**
 * Destroys game state main group.
 * Уничтожает главную группу игрового состояния.
 * @returns {void}
 */
TrinState.prototype.destroy = function() {
    this.group.destroy();
};

/**
 * Adds instance to game state main group.
 * Добавляет сущность в главную группу состояния.
 * @param {TrinBasic} entity Instance to add. Сущность для добавления.
 * @returns {void}
 */
TrinState.prototype.add = function(entity) {
    this.group.add(entity);
};

/**
 * Removes instance from game state main group.
 * Извлекает сущность из главной группы игрового состояния.
 * @param {TrinEntity} entity Instance for removing. Сущность для извлечения.
 * @returns {void}
 */
TrinState.prototype.remove = function(entity) {
    this.group.remove(entity);
};

/**
 * Calls when game screen resized.
 * Вызывается при изменений размеров окна.
 * @returns {void}
 */
TrinState.prototype.resized = function() {
};

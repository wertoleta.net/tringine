/**
 * Debugger class. (Класс дебагера.)
 * @param {Boolean} debug Debug enabled flag. (Вклюючен ли режим дебага)
 * @param {String} language Default debug messages language. Язык стандартных дебаг сообщений.
 * @returns {void}
 */
function TrinDebugger(debug, language) {
    if (debug === undefined)    {
        debug = false;
    }
    if (language === undefined || this.defaultMessages[language] === undefined) {
        language = "en";
    }
    this.debug = debug;
    this.language = language;
    this.messagesLog = [];
}

/**
 * Default debug messages. Стандартные дебаг сообщения.
 * @type Object
 */
TrinDebugger.prototype.defaultMessages  = {
    "en": {
        AnimationConstructorImageInvalid: "[Error] Parameter \"image\" is undefined or null. [TrinAnimation constructor]",
        AnimationImageAlreadyExists: "[Error] Image with name \"{0}\" already exists. [TrinAnimation addImage]",
        AnimationImageNotLoaded: "[Error] No such image with name \"{0}\". [TrinAnimation makeAnimation]",
        AnimationNotCreated: "[Error] No such animation with name \"{0}\". [TrinAnimation getAnimation]",
        AssetLoaderFileNotFound: "[Error] File \"{0}\" not found. [TrinAssetLoader loadNext]",
        SpriteNoAnimation: "[Error] No such animation with name \"{0}\". [TrinSprite switchAnimation]",
        CameraNoContext: "[Error] You are must set the context for camera. [TrinCamera constructor]",
        AnimationJsonNotLoaded: "[Error] No such JSON with name \"{0}\". [TrinAnimation makeAnimationFromAtlas]"
    }
};

/**
 * Add message to log. (Добавляет сообщение в лог.)
 * @param {String} message Message for adding to log. (Сообщение для добавления в лог.)
 * @param {Array} params Params for default log messages. Параметры для стандартных сообщений лога.
 * @returns {void}
 */
TrinDebugger.prototype.log = function(message, params) {
    if (this.defaultMessages[this.language][message] !== undefined)
    {
        message = this.defaultMessages[this.language][message];
        if (params !== undefined)   {
            for (var i = 0; i < params.length; i++)
            {
                message = message.replace("{" + i + "}", params[i]);
            }
        }
    }
    this.messagesLog[this.messagesLog.length] = message;
    if (this.debug) {
        this.outLog();
    }
};

/**
 * Out log. Выводит лог. 
 * @returns {void}
 */
TrinDebugger.prototype.outLog = function() {
    if (this.debug)
    {
        //Concatinate all messages in one string.
        //Объеденяем все сообщения в одну строку.
        var message = "";
        for (var i = 0; i < this.messagesLog.length; i++)
        {
            message += this.messagesLog[i] + "\n------";
        }
        
        //Out result string. 
        //Выводим результирующую строку.
        if (TrinGame.isMobile) {
            //If page have a #trinLog element - out log into them. Else just alert message.
            //Если на странице есть элемент #trinLog - выводим лог в него. Иначе просто делаем alert.
            var element = document.getElementById("trinLog");
            if (element !== null && element !== undefined) {
                element.innerHTML = message;
            } else {
                alert(message);
            }
        } else {
            console.log(message);
        }
    }
    
    this.clearLog();
};

/**
 * Clears the log. Очищает лог.
 * @returns {void}
 */
TrinDebugger.prototype.clearLog = function() {
    this.messagesLog = [];
};
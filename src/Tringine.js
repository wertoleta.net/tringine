/**
 * Global game variable.
 * @type Tringine
 */
var TrinGame;

/**
 * A main Tringine class.
 * Главный класс Tringine.
 * @param {Number} width
 * @param {Number} height
 * @param {TrinState} initialState
 * @param {Number} frameRate
 * @param {Number} minScreenWidth
 * @param {Number} minScreenHeight
 * @param {Number} api 
 * @returns {void}
 */
function Tringine(width, height, initialState, frameRate, minScreenWidth, minScreenHeight, api) {
    TrinGame = this;

    if (frameRate === undefined) {
        frameRate = 30;
    }
    if (minScreenWidth === undefined) {
        minScreenWidth = 640;
    }
    if (minScreenHeight === undefined) {
        minScreenHeight = 712;
    }
    if (api === undefined) {
        api = this.API_NONE;
    }
    this.width = width;
    this.height = height;
    this.minScreenWidth = minScreenWidth;
    this.minScreenHeight = minScreenHeight;
    this.frameRate = frameRate;

    TrinUtil.prototype.initIndex();

    this.gameContainer = document.getElementById("TrinGameContainer");
    this.canvas = document.getElementById("TrinGameCanvas");
    this.context = this.canvas.getContext("2d");
    this.scaleFactor = 1;
    this.state = null;
    this.device = TrinUtil.prototype.getDeviceType();
    this.browser = TrinUtil.prototype.getBrowserType();
    this.isChrome = TrinUtil.prototype.isChrome();
    this.isMobile = this.device !== TrinUtil.prototype.DEVICE_PC;
    this.mouse = new TrinMouse();
    this.widthToHeight = this.width / this.height;
    this.paused = false;
    this.debugger = new TrinDebugger(false);
    this.camera = new TrinCamera(this.context, 0, 0, this.width, this.height);
    this.api = api;
    this.rotateScreenState = null;
	this.screenRotated = false;
	this.gameState = null;
    this.timesResized = 0;
    this.offsetLeft = 0;
    this.offsetTop = 0;
    this.pauseSounds = null;
    this.unPauseSounds = null;
    this.lastSeen = Date.now();
    this.links = {
        moregames: function() {
            window.open("http://google.com", "_blank");
        },
        logo: function() {
            window.open("http://google.com", "_blank");
        },
        splash: function() {
            window.open("http://google.com", "_blank");
        }
    };
    this.switchState(new initialState());

    //Add resize eventListeners
    window.addEventListener('resize', function() {
        TrinGame.timesResized++;
        var f = function() {
            if (arguments.callee.time !== TrinGame.timesResized) {
                return;
            }
            window.scrollTo(0, 1);
            setTimeout(function() {
                TrinGame.resizeGame();
            }, 10);
        };
        f.time = TrinGame.timesResized;
        setTimeout(f, 10);
    }, false);
    window.addEventListener('orientationchange', function() {
        TrinGame.orientationChanged();
    }, false);

    //Resizing game
    setTimeout(function() {
        window.scrollTo(0, 1);
        setTimeout(function() {
            TrinGame.resizeGame();
        }, 10);
    }, 10);

    switch (this.api) {
        case this.API_NONE:
            this.start();
            break;
        case this.API_SOFTGAMES:
            if (SG_Hooks !== undefined) {
                this.lang = SG_Hooks.getLanguage(['ru', 'en', 'de', 'es', 'fr', 'it', 'pt', 'tr', 'th' , 'nl', 'pl']);
                SG_Hooks.setOrientationHandler( this.resizeGame );
                SG_Hooks.setResizeHandler( this.resizeGame );
            }
            this.links.moregames = "http://m.softgames.com/";
            this.start();
            break;
    }
    
    /*
     * Enables touch handlers in Chrome.
     * Включает обработку нажатий в хроме
     */
    if (this.device === TrinUtil.prototype.DEVICE_ANDROID)  {
        if (this.isChrome)  {
            window.addEventListener("touchstart", function(e){e.preventDefault();});
        }else{
            document.body.addEventListener("touchstart", function(e){e.preventDefault();});            
        }
    }
    var hidden = "hidden";
    
    var onchange =  function (evt) {
        var v = "visible", h = "hidden",
            evtMap = {
            focus:v, focusin:v, pageshow:v, blur:h, focusout:h, pagehide:h, visibilitychange: h, onblur:h, onfocus:v
            };

        evt = evt || window.event;
        if (evt.type in evtMap) {
            var vis = evtMap[evt.type] === v;
            if (evt.type === "visibilitychange")    {
                vis = !document[hidden];
            }
            if (!vis) {
               if (TrinGame.pauseSounds !== null)   {
                    TrinGame.pauseSounds();   
               }                
            }else{
               if (TrinGame.unPauseSounds !== null)   {
                    TrinGame.unPauseSounds();   
               }                
            }
        }
    }
    
    if (this.device === TrinUtil.prototype.DEVICE_IOS)  {
        window.onblur = onchange;
        window.onfocus = onchange;
    }	else	{
		// Standards:
		if (hidden in document)
			document.addEventListener("visibilitychange", onchange);
		else if ((hidden = "mozHidden") in document)
			document.addEventListener("mozvisibilitychange", onchange);
		else if ((hidden = "webkitHidden") in document)
			document.addEventListener("webkitvisibilitychange", onchange);
		else if ((hidden = "msHidden") in document)
			document.addEventListener("msvisibilitychange", onchange);
		// IE 9 and lower:
		else if ("onfocusin" in document)
			document.onfocusin = document.onfocusout = onchange;
		// All others:
		else
			window.onpageshow = window.onpagehide
				= window.onfocus = window.onblur = onchange;

		// set the initial state (but only if browser supports the Page Visibility API)
		if( document[hidden] !== undefined )
			onchange({type: document[hidden] ? "blur" : "focus"});
	}
}

/**
 * API type constants.
 * Константы типа API.
 * @type Number
 */
Tringine.prototype.API_NONE = 0;
Tringine.prototype.API_SPILGAMES = 1;
Tringine.prototype.API_SOFTGAMES = 2;

Tringine.prototype.update = function() {

    //Switching states
    if (this.nextState === null) {
        this.camera.clear();

        //Update state and draw it on canvas
        this.lastSeen = Date.now();
		if (this.screenRotated)	{
			if (this.rotateScreenState !== null) {
				this.rotateScreenState.update();
				this.rotateScreenState.draw(this.camera);
				this.mouse.update();
			} 
		}	else	if (this.state !== null && !this.paused) {
			TrinButton.prototype.oneButtonAlreadyHovered = false;
			this.state.update();
			this.state.draw(this.camera);
			this.mouse.update();
		} 		
	
    } else {
        if (this.state !== null) {
            this.state.destroy();
        }
        this.mouse.reset();
        this.state = this.nextState;
        this.nextState = null;
        this.state.create();
    }
};

Tringine.prototype.start = function() {
    setInterval(function() {
        TrinGame.update();
    }, 1000 / TrinGame.frameRate);
};

Tringine.prototype.switchState = function(newState) {
    this.nextState = newState;
};

Tringine.prototype.resizeGame = function() {
    //Calculate new size
    var innerWidth = window.innerWidth + 2;
    var innerHeight = window.innerHeight + 2;
    var maxWidthToHeight = this.width / this.minScreenHeight;
    var minWidthToHeight = this.minScreenWidth / this.height;
    var newWidth = innerWidth;
    var newHeight = innerHeight;
    var newWidthToHeight = newWidth / newHeight;
	var portrait = innerHeight > innerWidth;
    if (this.rotateScreenState !== null && this.device !== TrinUtil.prototype.DEVICE_PC) {
		this.screenRotated = portrait;// (window.orientation === 0 || window.orientation === 180);
    }
    if (newWidthToHeight > maxWidthToHeight) {
        newWidth = newHeight * maxWidthToHeight;
    } else if (newWidthToHeight < minWidthToHeight) {
        newHeight = newWidth / minWidthToHeight;
    } else {
        if (newWidthToHeight > this.width / this.height) {
            newWidth = newHeight * newWidthToHeight;
        } else {
            newHeight = newWidth / minWidthToHeight;
        }
    }

    newWidth = Math.min(newWidth, innerWidth);
    newHeight = Math.min(newHeight, innerHeight);
    newWidthToHeight = newWidth / newHeight;

    //Change container size
    this.gameContainer.style.marginLeft = -newWidth / 2 + 'px';
    this.gameContainer.style.marginTop = (-newHeight / 2) + 'px';
    this.gameContainer.style.width = newWidth + "px";
    this.gameContainer.style.height = newHeight + "px";
    
    this.offsetLeft = ((innerWidth - 2) - newWidth) / 2;
    this.offsetTop = ((innerHeight - 2) - newHeight) / 2;
    
    //Calculate visible area.
    var vw = this.width;
    var vh = this.height;
    if (newWidthToHeight < this.width / this.height) {
        vw = this.height * newWidthToHeight;
    } else if (newWidthToHeight > this.width / this.height) {
        vh = this.width / newWidthToHeight;
    }
    this.scaleFactor = newWidth / vw;
    this.camera.width = vw;
    this.camera.height = vh;
    this.camera.x = (this.width - vw) / 2;
    this.camera.y = (this.height - vh) / 2;

    if (Math.abs(this.canvas.width - vw) > 1 || Math.abs(this.canvas.height - vh) > 1) {
        this.canvas.width = vw;
        this.canvas.height = vh;
    }

    if (this.state !== null) {
        this.state.resized();
    }
    window.scrollTo(0, 1);
};

Tringine.prototype.orientationChanged = function() {
    this.resizeGame();
    //window.scrollTo(0, 1);
};

Tringine.prototype.globalPause = function() {
    this.paused = true;
};

Tringine.prototype.globalResume = function() {
    this.paused = false;
};

Tringine.prototype.gameStarted = function(){
    switch (this.api)   {
        case this.API_SOFTGAMES:
            SG_Hooks.start();
            break;
    }
};

Tringine.prototype.levelCompleted = function(level, score){
    switch (this.api)   {
        case this.API_SOFTGAMES:
            SG_Hooks.levelUp(level, score);
            break;
    }
};

Tringine.prototype.openMoreGamesLink = function()   {
    window.open(TrinGame.links.moregames, "_blank");   
}

Tringine.prototype.soundPlayed = function(){
    if(Date.now() - TrinGame.lastSeen > 2000){
        TrinGame.pauseSounds();
    }
};
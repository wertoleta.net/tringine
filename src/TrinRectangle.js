/**
 * Just a rectangle entity. Have x, y, width and height. 
 * Просто сущность прямоугольника. Имеет Х, Y, ширину и высоту.
 * @param {Number} x
 * @param {Number} y
 * @param {Number} width
 * @param {Number} height
 * @returns {TrinRectangle}
 */
function	TrinRectangle(x, y, width, height) {
    if (x === undefined)    { x = 0;}
    if (y === undefined)    { y = 0;}
    if (width === undefined)    { width = 0;}
    if (height === undefined)    { height = 0;}
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

/**
 * Top side of rectangle.
 * Верхняя сторона прямоугольника.
 * @returns {Number} Y coordinate of top side of the rectangle. Y координата верхней стороны прямоугольника.
 */
TrinRectangle.prototype.top = function() {
    return this.y;
};

/**
 * Left side of rectangle.
 * Левая сторона прямоугольника.
 * @returns {Number} X coordinate of left side of the rectangle. Х координата левой стороны прямоугольника.
 */
TrinRectangle.prototype.left = function() {
    return this.x;
};

/**
 * Bottom side of rectangle.
 * Нижняя сторона прямоугольника.
 * @returns {Number} Y coordinate of bottom side of the rectangle. Y координата нижней стороны прямоугольника.
 */
TrinRectangle.prototype.bottom = function() {
    return this.y + this.height;
};

/**
 * Right side of rectangle.
 * Правая сторона прямоугольника.
 * @returns {Number} X coordinate of right side of the rectangle. Х координата правой стороны прямоугольника.
 */
TrinRectangle.prototype.right = function() {
    return this.x + this.width;
};

/**
 * Set position and size of rectangle.
 * Устанавливает позицию и размер прямоугольника.
 * @param {Number} x New X coordinate of the rectangle. Новая позиция по Х прямоугольника. 
 * @param {Number} y New Y coordinate of the rectangle. Новая позиция по Y прямоугольника.
 * @param {Number} width New width of the rectange. Новая ширина прямоугольника.
 * @param {Number} height New height of the rectangle. Новая высота прямоугольника.
 * @returns {void}
 */
TrinRectangle.prototype.set = function(x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
};


/**
 * Check intersects with rectangle.
 * Проверяет пересечение с прямоугольником.
 * @param {Number} x X coordinate of other rectangle. Х координата другого прямоугольника.
 * @param {Number} y Y coordinate of other rectangle. У координата другого прямоугольника.
 * @param {Number} width Other rectangle width size... ШирИна другого прямоугольника.
 * @param {Number} height Other rectangle height. Высота другого прямоугольника.
 * @returns {Boolean}
 */
TrinRectangle.prototype.intersects = function(x, y, width, height) {
    if (width === undefined && height === undefined) {
        return (x > this.left() && x < this.right() && y > this.top() && y < this.bottom());
    }
    var t = y;
    var r = x + width;
    var b = y + height;
    var l = x;

    return (r > this.left() && l < this.right()) && (b > this.top() && t < this.bottom());
};


/**
 * Check intersects with rectangle.
 * Проверяет пересечение с прямоугольником.
 * @param {TrinRectangle} rectangle Other rectangle. Другой прямоугольник.
 * @returns {Boolean}
 */
TrinRectangle.prototype.intersectsRect = function(rectangle) {
    return this.intersects(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
};

/**
 * Utility class to handle mouse and touch events.
 * Класс для обработки событий мыши и касаний.
 * @returns {void}
 */
function TrinMouse() {
    this.x = 0;
    this.y = 0;
    this.current = 0;
    this.last = 0;
    this.out = false;
    this.rawX = 0;
    this.rawY = 0;
	this.whell = 0;
    this.usePointerOnTouch = false;
    try {
        this.isxdk = (intel !== undefined && intel.xdk.isxdk);
    }catch(e){
        this.isxdk = false;
    }
    this.getMouseXY.mouse = this;
    
    if (TrinGame.isMobile && !this.isxdk) {
        this.onTouchStart.mouse = this;
        this.onTouchEnd.mouse = this;
        TrinGame.canvas.addEventListener('touchmove', this.getMouseXY, false);
        TrinGame.canvas.addEventListener('touchstart', this.onTouchStart, false);
        TrinGame.canvas.addEventListener('touchend', this.onTouchEnd, false);
    } else {
        this.onMouseDown.mouse = this;
        this.onMouseUp.mouse = this;
        TrinGame.canvas.addEventListener('mousedown', this.onMouseDown, false);
        TrinGame.canvas.addEventListener('mouseup', this.onMouseUp, false);
        TrinGame.canvas.addEventListener('mousemove', this.getMouseXY, false);
        TrinGame.canvas.addEventListener('mouseout', this.onMouseUp, false);
		TrinGame.canvas.addEventListener("mousewheel", this.onMouseWhell, false);
		TrinGame.canvas.addEventListener("DOMMouseScroll", this.onMouseWhell, false);
    }
}

/**
 * Updates mouse position and state.
 * Обновляет позицию и состояние курсора.
 * @returns {void}
 */
TrinMouse.prototype.update = function() {
    this.x = Math.min(Math.floor(this.rawX), TrinGame.width);
    this.y = Math.min(Math.floor(this.rawY), TrinGame.height);
    if (this.current === -1) {
        this.current = 0;
    } else if (this.current === 2) {
        this.current = 1;
    }
	this.whell = 0;
    this.last = this.current;
};

/**
 * Get pointer position relative to canvas left-top position.
 * Получает позицию курсора относительно левого-верхнего угла канваса.
 * @param {MouseEvent} event Mouse event. Событиу мыши. 
 * @returns {Boolean}
 */
TrinMouse.prototype.getMouseXY = function(event) {
	event.preventDefault();
    var mouse = arguments.callee.mouse;
    var mx;
    var my;
    if (TrinGame.device === TrinUtil.prototype.DEVICE_ANDROID && !mouse.isxdk) {
        if (TrinGame.isChrome)  {
            mx = (event.touches[0].pageX - TrinGame.offsetLeft) / TrinGame.scaleFactor;
            my = (event.touches[0].pageY - TrinGame.offsetTop) / TrinGame.scaleFactor;
        }   else    {
            mx = (event.touches[0].pageX + event.layerX) / TrinGame.scaleFactor;
            my = (event.touches[0].pageY + event.layerY) / TrinGame.scaleFactor;            
        }
    } else {
        if (event.offsetX === undefined) {
            mx = event.layerX / TrinGame.scaleFactor;
            my = event.layerY / TrinGame.scaleFactor;
        } else {
            mx = event.offsetX / TrinGame.scaleFactor;
            my = event.offsetY / TrinGame.scaleFactor;
        }
    }
    mouse.rawX = mx;
    mouse.rawY = my; 
    return false;
};

/**
 * Return true if mouse is pressed.
 * Возвращает true если кнопка мыши опущена.
 * @returns {Boolean}
 */
TrinMouse.prototype.isDown = function() {
    return this.current > 0;
};

/**
 * Return true if mouse is pressed on this step.
 * Возвращает true если кнопка мыши опущена в этот шаг.
 * @returns {Boolean}
 */
TrinMouse.prototype.isPressed = function() {
    return this.current === 2;
};


/**
 * Return true if mouse is released on this step.
 * Возвращает true если кнопка мыши отпущена в этот шаг.
 * @returns {Boolean}
 */
TrinMouse.prototype.isReleased = function() {
    return this.current === -1;
};

/**
 * Mouse down handler
 * Обработчик нажатия на кнопку мыши.
 * @returns {void}
 */
TrinMouse.prototype.onMouseDown = function() {
    var mouse = arguments.callee.mouse;
    mouse.current = (mouse.current > 0) ? 1 : 2;
};

/**
 * Mouse up handler
 * Обработчик отпускания кнопки мыши.
 * @returns {void}
 */
TrinMouse.prototype.onMouseUp = function() {
    var mouse = arguments.callee.mouse;
    mouse.current = (mouse.current > 0) ? -1 : 0;
};

/**
 * Touch start handler.
 * Обработчик начала касания.
 * @param {TouchEvent} event Touch event. Событие касания.
 * @returns {Boolean}
 */
TrinMouse.prototype.onTouchStart = function(event) {
    var mouse = arguments.callee.mouse;
    if (TrinGame.device === TrinUtil.prototype.DEVICE_ANDROID) {
        event.preventDefault();
        if (TrinGame.isChrome)  {
            mouse.rawX = mouse.x = (event.changedTouches[0].pageX - TrinGame.offsetLeft) / TrinGame.scaleFactor + TrinGame.camera.x;
            mouse.rawY = mouse.y = (event.changedTouches[0].pageY - TrinGame.offsetTop) / TrinGame.scaleFactor + TrinGame.camera.y;
        }   else    {
            mouse.rawX = mouse.x = (event.changedTouches[0].pageX + event.layerX) / TrinGame.scaleFactor + TrinGame.camera.x;
            mouse.rawY = mouse.y = (event.changedTouches[0].pageY + event.layerY) / TrinGame.scaleFactor + TrinGame.camera.y;         
        }
    } else {
        mouse.rawX = mouse.x = event.layerX / TrinGame.scaleFactor;
        mouse.rawY = mouse.y = event.layerY / TrinGame.scaleFactor;
    }
    mouse.current = (mouse.current > 0) ? 1 : 2;
    return false;
};

/**
 * Touch end handler.
 * Оброботчик окончания каания.
 * @returns {Boolean}
 */
TrinMouse.prototype.onTouchEnd = function() {
    var mouse = arguments.callee.mouse;
    mouse.current = (mouse.current > 0) ? -1 : 0;
    if (!this.usePointerOnTouch)
    {
        mouse.rawX = -999;
        mouse.rawY = -999;
    }
    return false;
};

/**
 * Reset mouse button state.
 * Сбросить состояние нопки мыши.
 * @returns {void}
 */
TrinMouse.prototype.reset = function() {
    this.current = 0;
};

TrinMouse.prototype.mouseWhell = function(e) {
    var e = window.event || e;
	this.whell += Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
};
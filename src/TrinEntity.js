/**
 * Class for objects with size.
 * Класс для сущьностей у которых есть размер.
 * @returns {void}
 */
function TrinEntity() {
    TrinEntity.super.constructor.apply(this);
    this.x = 0;
    this.y = 0;
    this.width = 0;
    this.height = 0;
    this.origin = {x: 0, y: 0};
    this.bounds = new TrinRectangle(0, 0, 0, 0);
    this.boundsOffset = {left: 0, top: 0, right: 0, bottom: 0};
    this.scale = {x: 1, y: 1};
    this.alpha = 1;
    this.angle = 0;
    this.shadow = {x: 0, y: 0, blur: 0, color: "#000000"};
}

extend(TrinEntity, TrinBasic);


/**
 * Destroy and cleen the instance. 
 * Уничтожает и очищает сущьность
 * @returns {void}
 */
TrinEntity.prototype.destroy = function() {
    TrinEntity.super.destroy.apply(this);
    this.origin = null;
    this.bounds = null;
    this.boundsOffset = null;
    this.scale = null;
};


/**
 * Update the bounds of instance. Running every step after update. 
 * Обновляет границы сущности. Выполняется каждый шаг после update. 
 * @returns {void}
 */
TrinEntity.prototype.postUpdate = function() {
    TrinEntity.super.postUpdate.apply(this);
    this.updateBounds();
};

/**
 * Test for hits in position {x,y} with instance bounds.
 * Проверка на столкновение в позиций {x, y} с границами сущьности.
 * @param {Integer} x X coordinate of hit-test. Х координата для проверки столкновения.
 * @param {Integer} y Y coordinate of hit-test. Y координата для проверки столкновения.
 * @returns {Boolean}
 */
TrinEntity.prototype.hitTest = function(x, y) {
    return this.bounds.intersects(x, y);
};

/**
 * Updates the instance bounds. 
 * Обновляет границы сущности.
 * @returns {void}
 */
TrinEntity.prototype.updateBounds = function() {
    this.bounds.set(this.x - this.origin.x * this.scale.x + this.boundsOffset.left,
            this.y - this.origin.y * this.scale.y + this.boundsOffset.top,
            (this.width - this.boundsOffset.right - this.boundsOffset.left) * this.scale.x,
            (this.height - this.boundsOffset.bottom - this.boundsOffset.top) * this.scale.y);
};

/**
 * Moves instance on some values. 
 * Двигает сущьность на некоторую величину.
 * @param {Number} x The X value of translation. Смещение по оси Х. 
 * @param {Number} y The Y value of translation. Смещение по оси Y. 
 * @returns {void}
 */
TrinEntity.prototype.move = function(x, y) {
    this.reset(this.x + x, this.y + y);
};

/**
 * Moves instance to absolute position. 
 * Двигает сущьность в определенную точку.
 * @param {Number} x The X value of destination point. Координата точки назначения по оси Х. 
 * @param {Number} y The Y value of destination point. Координата точки назначения по оси Y. 
 * @returns {void}
 */
TrinEntity.prototype.reset = function(x, y) {
    this.x = x;
    this.y = y;
};

TrinEntity.prototype.center = function(axis) {
    if (axis === undefined) {
        axis = "xy";
    }
    if (axis.indexOf("x") > -1) {
        this.origin.x = this.width / 2;
    }
    if (axis.indexOf("y") > -1) {
        this.origin.y = this.height / 2;
    }
};

TrinEntity.prototype.draw = function(camera) {
    TrinEntity.super.draw.apply(this, [camera]);
    var context = camera.context;
    context.restore();
    context.save();
    if (this.angle !== 0) {
        context.translate(this.x - camera.x, this.y - camera.y);
        context.rotate(this.angle);
    }
    context.globalAlpha = this.alpha;

    context.shadowOffsetX = this.shadow.x;
    context.shadowOffsetY = this.shadow.y;
    context.shadowBlur = this.shadow.blur;
    context.shadowColor = this.shadow.color;
};
/**
 * Use it for saves.
 * Используйте этот класс для сохранений.
 * @returns {void}
 */
function TrinStorage() {
}

/**
 * True if you can save something. False if you can't.
 * Показывает, можем ли мы сохранять данные.
 * @type Boolean
 */
TrinStorage.prototype.supportsStorage = false;

/**
 * Checks local storage avaiablity.
 * Проверяет, доступно ли локальное хранилище.
 * @returns {void}
 */
TrinStorage.prototype.init = function()   {
    try {
        TrinStorage.prototype.supportsStorage = 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        TrinStorage.prototype.supportsStorage = false;
    }
};

/**
 * Save value to storage.
 * Сохранить значение в хранилише.
 * @param {String} name Value key. Ключ для сохранения.
 * @param {String} value Value. Значение.
 * @returns {void}
 */
TrinStorage.prototype.save = function(name, value) {
    if (this.supportsStorage) {
        try {
            localStorage.setItem(name, value);
        } catch (e) {
            this.supportsStorage = false;
        }
    }
};

/**
 * Load value from storage by key.
 * Загружает значение из хранилища по ключу.
 * @param {String} name Value key. Ключ значения.
 * @returns {String}
 */
TrinStorage.prototype.load = function(name) {
    if (this.supportsStorage) {
        try {
            return localStorage.getItem(name);
        } catch (e) {
            this.supportsStorage = false;
        }
    }
    return null;
};

/**
 * Remove value from storage by key.
 * Убрать значение из хранилища по ключу. 
 * @param {String} name Value key. Ключ значения.
 * @returns {void}
 */
TrinStorage.prototype.remove = function(name) {
    if (this.supportsStorage) {
        try {
            localStorage.removeItem(name);
        } catch (e) {
            this.supportsStorage = false;
        }
    }
};

/**
 * Removes all storaged values.
 * Очищает все ранее сохраненые значения.
 * @returns {void}
 */
TrinStorage.prototype.clear = function() {
    if (this.supportsStorage) {
        try {
            localStorage.clear();
        } catch (e) {
            this.supportsStorage = false;
        }
    }
};

/**
 * Length of storaged elements.
 * Количество сохраненных величин.
 * @returns {Number}
 */
TrinStorage.prototype.length = function() {
    if (this.supportsStorage) {
        try {
            return localStorage.length;
        } catch (e) {
            this.supportsStorage = false;
        }
    }
    return 0;
};

/**
 * Get key by position in storage.
 * Получить ключ значения по месту в хранилище. 
 * @param {Integer} index Value index. Номер значения в хранилище.
 * @returns {String}
 */
TrinStorage.prototype.getKey = function(index) {
    if (this.supportsStorage && index >= 0 && index < this.length) {
        try {
            return localStorage.key(key);
        } catch (e) {
            this.supportsStorage = false;
        }
    }
    return null;
};
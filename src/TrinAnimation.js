/**
 * Animation class. Have a link to the image and list of rectangles (frames) from this image.  
 * Класс анимаций. Представляет собой ссылку на изображение и список областей (кадров) из этого изображения.
 * @param {Image} image Animation image (Изображение)
 * @param {String} name Animation name (Имя создаваемой анимации)
 * @returns void
 */
function TrinAnimation(image, name) {
    if (image === undefined) {
        TrinGame.debugger.log("AnimationConstructorImageInvalid");
    }
    if (name === undefined) {
        name = "noname";
    }
    this.image = image;
    this.frames = [];
    this.name = name;
    this.totalFrames = 0;
    this.width = 0;
    this.height = 0;
}

/**
 * Images and animations arrays. 
 * Массивы для изображений и анимаций.
 * @type Array
 */
TrinAnimation.prototype.IMAGES = [];
TrinAnimation.prototype.ANIMATIONS = [];

/**
 * Adds loaded image to array. 
 * Добавляет загруженное изображение в контейнер.
 * @param {Image} image
 * @returns {void}
 */
TrinAnimation.prototype.addImage = function(image) {
    if (TrinAnimation.prototype.IMAGES[image.info.name] === undefined) {
        TrinAnimation.prototype.IMAGES[image.info.name] = image;
    } else {
        TrinGame.debugger.log("AnimationImageAlreadyExists", [image.info.name]);
    }
};

/**
 * Creates animation from gridded image. 
 * Создает анимацию из изображения разбитого на ячейки.
 * @param {String} imageName Name of image. Название изображения.
 * @param {String} animationName Name for new animation. Название новой анимаций.
 * @param {Integer} frameWidth Frame width (Grid cell width). Ширина кадра (Ширина ячейки).
 * @param {Integer} frameHeight Frame height (Grid cell height). Высота кадра (Высота ячейки)
 * @param {Array of Integer} frames Array of frames numbers. Массив номеров кадров.
 * @returns {void}
 */
TrinAnimation.prototype.makeAnimation = function(imageName, animationName, frameWidth, frameHeight, frames) {
    var image = TrinAnimation.prototype.IMAGES[imageName];
    if (image === undefined || image === null) {
        TrinGame.debugger.log("AnimationImageNotLoaded", [imageName]);
        return;
    }
    if (frameWidth === undefined) {
        frameWidth = image.width;
    }
    if (frameHeight === undefined) {
        frameHeight = image.height;
    }
    if (frames === undefined || frames.length === 0) {
        frames = [0];
    }
    var animation = new TrinAnimation(image, animationName);
    var rows = Math.floor(image.height / frameHeight);
    var columns = Math.floor(image.width / frameWidth);
    for (var i = 0; i < frames.length; i++) {
        var frameNum = frames[i];
        var frame = {x: (frameNum % columns) * frameWidth, y: Math.floor(frameNum / columns) * frameHeight, width: frameWidth, height: frameHeight, offset: {x: 0, y: 0}};
        animation.frames.push(frame);
    }
    animation.totalFrames = animation.frames.length;
    TrinAnimation.prototype.ANIMATIONS[animationName] = animation;
};

TrinAnimation.prototype.makeAnimationFromAtlas = function(imageName, jsonName) {
    var image = TrinAnimation.prototype.IMAGES[imageName];
    if (image === undefined || image === null) {
        TrinGame.debugger.log("AnimationImageNotLoaded", [imageName]);
        return;
    }
    if (jsonName === undefined) {
        jsonName = imageName;
    }
    var json = TrinUtil.prototype.LOADED_JSON[jsonName];
    if (json === undefined || json === null) {
        TrinGame.debugger.log("AnimationJsonNotLoaded", [jsonName]);
        return;
    }
    var atlas = json.atlas;
    var animation;
    var animationJson;
    var frames;
    for(var i = 0; i < atlas.animations.length; i++)    {
        animationJson = atlas.animations[i];
        animation = new TrinAnimation(image, animationJson.name);
        frames = animationJson.frames;
        var offsetMin = {x: 10000, y: 10000};
        var frame;
        for (var j = 0; j < frames.length; j++) {
            frame = frames[j];
            offsetMin.x = Math.min(offsetMin.x, frame.offsetX);
            offsetMin.y = Math.min(offsetMin.y, frame.offsetY);
        }
        for (var j = 0; j < frames.length; j++)   {
            frame = {
                x: frames[j].x,
                y: frames[j].y, 
                width: frames[j].width, 
                height: frames[j].height, 
                offset: {
                    x: frames[j].offsetX - offsetMin.x,
                    y: frames[j].offsetY - offsetMin.y
                }
            };
            animation.frames.push(frame);
        }
        animation.totalFrames = frames.length;
        TrinAnimation.prototype.ANIMATIONS[animation.name] = animation;
    }
};

/**
 * Returns animation by name. 
 * Возвращает анимацию по ее имени.
 * @param {String} name Name of animation. Имя анимации.
 * @returns {TrinAnimation}
 */
TrinAnimation.prototype.getAnimation = function(name) {
    if (TrinAnimation.prototype.ANIMATIONS[name] === undefined) {
        TrinGame.debugger.log("AnimationNotCreated", [name]);
        return null;
    }
    return TrinAnimation.prototype.ANIMATIONS[name];
};

/**
 * Destroy and clear instance. 
 * Уничтожает и очищает сущьность.
 * @returns {void}
 */
TrinAnimation.prototype.destroy = function() {
    this.frames = null;
    this.image = null;
};
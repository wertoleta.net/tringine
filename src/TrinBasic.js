/**
 * Basic class in Tringine objects hierarchy. 
 * Базовый класс в иерархий объектов Tringine.
 * @returns {void}
 */
function TrinBasic() {
    this.active = true;
    this.exists = true;
    this.visible = true;
    this.parent = null;
    this.alive = true;
}

/**
 * Destroy and cleen the instance. 
 * Уничтожает и очищает сущьность
 * @returns {void}
 */
TrinBasic.prototype.destroy = function() {
};

/**
 * Update the instance. Running every step.
 * Обновляет сущность. Выполняется каждый шаг.
 * @returns {void}
 */
TrinBasic.prototype.update = function() {
};

/**
 * Update the instance. Running every step after update.
 * Обновляет сущность. Выполняется каждый шаг после update.
 * @returns {void}
 */
TrinBasic.prototype.postUpdate = function() {
};

/**
 * Draw the instance. Running every step after postUpdate.
 * Рисует сущьность. Выполняется каждый шаг.
 * @param {TrinCamera} camera Camera instance for drawing. Сущьность камеры на которую рисовать.
 * @returns {void}
 */
TrinBasic.prototype.draw = function(camera) {
};

/**
 * Kills the instance (Not destroy). 
 * Убивает сущьность (Не уничтожает).
 * @returns {void}
 */
TrinBasic.prototype.kill = function() {
    this.exists = false;
    this.alive = false;
};

/**
 * Revive the instance.
 * Воскрещает сущьность. Прям как Ийсус.
 * @returns {void}
 */
TrinBasic.prototype.revive = function() {
    this.exists = true;
    this.alive = true;
};

/**
 * Test instance for hits in position {x,y}
 * Проверка на столкновение в позиций {x, y}.
 * @param {Integer} x X coordinate of hit-test. Х координата для проверки столкновения.
 * @param {Integer} y Y coordinate of hit-test. Y координата для проверки столкновения.
 * @returns {Boolean}
 */
TrinBasic.prototype.hitTest = function(x, y) {
    return false;
};
/**
 * !!!ATTENTION!!! Sounds is fully alfa-beta version. Best way - don't use this.
 * !!!ВНИМАНИЕ!!! Звуки в полной пре-альфа-бета версий. Лучший путь - не ипользовать их. * 
 * @returns {TrinSound}
 */
function TrinSound() {
}

/**
 * Global sound list.
 * Глобальный список звуков.
 * @type Array
 */
TrinSound.prototype.SOUNDS = [];
TrinSound.prototype.withMusic   = false;
TrinSound.prototype.withSounds  = false;

TrinSound.prototype.mute = false;

/**
 * Adds sound to list after loading.
 * Добавляет звук в список после загрузки.
 * @param {Audio} sound Sound to add. Звук для добавления.
 * @returns {void}
 */
TrinSound.prototype.add = function(sound) {
    this.SOUNDS[sound.info.name] = sound;
    sound.muted = false;
};

TrinSound.prototype.remove = function(sound) {
    this.SOUNDS[sound.info.name] = undefined;
};

/**
 * Returns sound from list by name.
 * Возвращает звук из списка по имени.
 * @param {String} name
 * @returns {Audio}
 */
TrinSound.prototype.get = function(name) {
    if (this.SOUNDS[name] === undefined) {
        return null;
    }
    return this.SOUNDS[name];
};


/**
 * Mutes all sounds.
 * Ставит на паузу все звуки.
 *  @returns {void}
 */
TrinSound.prototype.muteAll = function() {
    var sounds = TrinSound.prototype.SOUNDS;
    for (var key in sounds) {
        if (sounds.hasOwnProperty(key)) {
            var sound = TrinSound.prototype.get(key);
            if (sound !== null)  {
                sound.trinMuted = true;
                sound.muted = true;
                sound.pause();
            }
        }
    }
};

/**
 * Start playing all paused sounds.
 * Воспроизводит все звуки на паузе.
 * @returns {void}
 */
TrinSound.prototype.unMuteAll = function() {
    var sounds = TrinSound.prototype.SOUNDS;
    for (var key in sounds) {
        if (sounds.hasOwnProperty(key)) {
            var sound = TrinSound.prototype.get(key);
            if (sound != null && sound.trinMuted)    {
                sound.trinMuted = false;
                sound.muted = false;
            }
        }
    }
};

TrinSound.prototype.play = function(name, forced) {
    if (this.mute || this.SOUNDS[name] === undefined)   {
        return;
    }
    if (forced === undefined || forced === true)    {
        this.SOUNDS[name].currentTime = 0;
    }
    this.SOUNDS[name].play();
    if (this.SOUNDS[name].loop) {
        this.SOUNDS[name].ontimeupdate = TrinGame.soundPlayed;   
    }
};

TrinSound.prototype.setVolume = function(name, volume) {
    if (this.mute || this.SOUNDS[name] === undefined)   {
        return;
    }
    this.SOUNDS[name].volume = volume;
};

TrinSound.prototype.setLoop = function(name, loop) {
    if (this.mute || this.SOUNDS[name] === undefined)   {
        return;
    }
    this.SOUNDS[name].loop = loop;
};

TrinSound.prototype.stop = function(name) {
    if (this.SOUNDS[name] !== undefined) {
        this.SOUNDS[name].pause();
        this.SOUNDS[name].currentTime = 0;
    }
};

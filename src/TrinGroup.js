/**
 * This class draws its childs to separated canvas. After draw this canvas on parent canvas. 
 * Use like surface for grouping static elements.
 * Рисует детей на отдельный канвас. Потом рисует этот канвас на родительский.
 * Используется как сурфейс (Сурфейсы добавятся позже) для групировки статичных элементов.
 * @param {Number} width Width of new canvas. Ширина создаваемого канваса.
 * @param {Number} height Height of new canvas. Высота создаваемого канваса.
 * @returns {void}
 */
function TrinGroup(width, height) {
    TrinGroup.super.constructor.apply(this);
    var canvas = document.createElement("canvas");
    canvas.width = width;
    canvas.height = height;
    this.canvas = canvas;
    var context = canvas.getContext("2d");
    
    this.layer = new TrinLayer();
    this.width = width;
    this.height = height;
    this.camera = new TrinCamera(context, 0, 0, width, height);
    this.updateBounds();
    
    this.group = new TrinLayer();
}

extend(TrinGroup, TrinEntity);

TrinGroup.prototype.update = function() {
    this.childrensToGlobal();
    TrinGroup.super.update.apply(this);
    this.group.update();
    this.childrensToLocal();
};

/**
 * Adds instance to group.
 * Добавляет сущьность в группу.
 * @param {TrinBasic} entity Instance to add. Сущность для добавления.
 * @returns {void}
 */
TrinGroup.prototype.add = function(entity) {
    this.group.add(entity);
};

/**
 * Removes instance from group.
 * Убирает сущьность из группы.
 * @param {TrinBasic} entity Instance to remove. Сущность для изъятия.
 * @returns {void}
 */
TrinGroup.prototype.remove = function(entity) {
    this.group.remove(entity);
};

TrinGroup.prototype.makeDraw = function()   {
    this.camera.context.clearRect(0 ,0, this.width, this.height);
    this.group.draw(this.camera);  
};

/**
 * Draw group to camera.
 * Рисует групу.
 * @param {TrinCamera} camera Camera to draw on. Камера для рисования.
 * @returns {void}
 */
TrinGroup.prototype.draw = function(camera) {
    TrinGroup.super.draw.apply(this, [camera]);
    
    var sx = 0;
    var sy = 0;
    var sw = this.width;
    var sh = this.height;
    var dx = this.x - this.origin.x * this.scale.x - camera.x;
    var dy = this.y - this.origin.y * this.scale.y - camera.y;
    var dw = this.width * this.scale.x;
    var dh = this.height * this.scale.y;
    camera.context.drawImage(this.camera.context.canvas, sx, sy, sw, sh, dx, dy, dw, dh);
};

TrinGroup.prototype.childrensToGlobal = function()  {
    var f = function(x, y){
        this.x += x;
        this.y += y;
        this.updateBounds();
    };
    this.group.callAll(f, [this.x -this.origin.x, this.y-this.origin.y]);
};

TrinGroup.prototype.childrensToLocal = function()  {
    var f = function(x, y){
        this.x -= x;
        this.y -= y;
        this.updateBounds();
    };
    this.group.callAll(f, [this.x - this.origin.x, this.y-this.origin.y]);
};
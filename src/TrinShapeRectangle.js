/**
 * Entity draws rectangle.
 * Сущность рисует прямоугольник.
 * @param {Number} x Left side coordinate. Координата левой стороны прямоугольника.
 * @param {Number} y Top side coordinate. Координата верхней стороны прямоугольника.
 * @param {Number} width Width of the rectangle. Ширина прямоугольника.
 * @param {Number} height Height of the rectangle. Высота прямоугольника.
 * @param {String} color Color of the rectangle. Цвет прямоугольника.
 * @param {String} fillColor Color of the rectangle fill. Цвет заливки прямоугольника.
 * @returns {void}
 */
function TrinShapeRectangle(x, y, width, height, color, fillColor) {
    TrinShapeRectangle.super.constructor.apply(this);
    if (x === undefined) {
        x = 0;
    }
    if (y === undefined) {
        y = 0;
    }
    if (width === undefined) {
        width = 10;
    }
    if (height === undefined) {
        height = 10;
    }
    if (color === undefined) {
        color = "#ffffff";
    }
    if (fillColor === undefined) {
        fillColor = null;
    }
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.color = color;
    this.fillColor = fillColor;
	this.thickness = 1;
}

extend(TrinShapeRectangle, TrinEntity);

/**
 * Draw the rectangle to camera.
 * Рисует прямоугольник на камеру.
 * @param {TrinCamera} camera
 * @returns {void}
 */
TrinShapeRectangle.prototype.draw = function(camera) {
    TrinShapeRectangle.super.draw.apply(this, [camera]);
    var context = camera.context;
    context.beginPath();
    context.globalAlpha = this.alpha;
	context.lineWidth = this.thickness;
    if (this.fillColor !== null) {
        context.strokeStyle = this.color;    
        context.fillStyle = this.fillColor;    
        context.rect(this.x - camera.x, this.y - camera.y, this.width, this.height);
        context.fill();
    } else {
        context.strokeStyle = this.color;
        context.rect(this.x - camera.x, this.y - camera.y, this.width, this.height);
    }
    context.stroke();
};

/**
 * Check intersects with rectangle.
 * Проверяет пересечение с прямоугольником.
 * @param {Number} x X coordinate of other rectangle. Х координата другого прямоугольника.
 * @param {Number} y Y coordinate of other rectangle. У координата другого прямоугольника.
 * @param {Number} w Other rectangle width size... ШирИна другого прямоугольника.
 * @param {Number} h Other rectangle height. Высота другого прямоугольника.
 * @returns {Boolean}
 */
TrinShapeRectangle.prototype.intersects = function(x, y, w, h) {
    var rect = new TrinRectangle(this.x, this.y, this.width, this.height);
    return rect.intersects(x, y, w, h);
};

/**
 * Check intersects with rectangle.
 * Проверяет пересечение с прямоугольником.
 * @param {TrinRectangle} rectangle Other rectangle. Другой прямоугольник.
 * @returns {Boolean}
 */
TrinShapeRectangle.prototype.intersectsRect = function(rectangle)  {
    return rectangle.intersects(this.x, this.y, this.width, this.height);
}; 
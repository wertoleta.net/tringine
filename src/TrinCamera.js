function TrinCamera(context, x, y, width, height)   {
    TrinCamera.super.constructor.apply(this, [x, y, width, height]);
    if (context === undefined) {
        TrinGame.debugger.log("CameraNoContext");
    }
    this.context = context;
    this.context.save();
    this.backgroundColor = null;
}

extend(TrinCamera, TrinRectangle);

TrinCamera.prototype.clear = function()    {
	this.context.restore();
    if (this.backgroundColor === null)  {
        this.context.clearRect(0, 0, this.width, this.height);
    }
    else
    {
        this.context.fillStyle = this.backgroundColor;
        this.context.fillRect(0, 0, this.width, this.height);
    }
}
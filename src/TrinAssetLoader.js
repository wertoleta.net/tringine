/**
 * Class for loading Images, Sounds (beta) and JSON. 
 * Класс для загрузки изображений, звуков (бета) и файлов JSON. 
 * @param {Function} onComplete Complete callback. Функция выполняема по завершений загрузки.
 * @param {Function} onProgress Progress callback. Функция выполняемая при продвижений загрузки.
 * @returns {TrinAssetLoader}
 */
function TrinAssetLoader(onComplete, onProgress) {
    if (onComplete === undefined) {
        onComplete = null;
    }
    if (onProgress === undefined) {
        onProgress = null;
    }
    this.currentAsset = 0;
    this.assetsToLoad = [];
    this.onComplete = onComplete;
    this.onProgress = onProgress;
    this.caller = null;
    this.partSize = 0;
    this.currentProgress = 0;
}

/**
 * Asset type constants. 
 * Константы типа загружаемого ресурса.
 * @type Number
 */
TrinAssetLoader.prototype.TYPE_IMAGE = 0;
TrinAssetLoader.prototype.TYPE_SOUND = 1;
TrinAssetLoader.prototype.TYPE_JSON = 2;

/**
 * Add image to loading queue. 
 * Добавляет изображение в очередь загруки.
 * @param {String} imageName Name of loaded image. Имя загружаемого изображения.
 * @param {String} imageUrl URL path to image. Путь загружаемого изображения.
 * @param {Boolean} isAnimated
 * @returns {void}
 */
TrinAssetLoader.prototype.addImage = function(imageName, imageUrl, isAnimated) {
    if (isAnimated === undefined) {
        isAnimated = false;
    }
    if (TrinGame.debugger.debug)    {
        imageUrl += "?rnd=" + Math.random();
    }
    this.assetsToLoad.push({
        name: imageName,
        src: imageUrl,
        type: this.TYPE_IMAGE,
        animated: isAnimated
    });
};

/**
 * Add sound to loading queue. 
 * Добавляет звук в очередь загрузки.
 * @param {String} soundName Name of sound. Имя загружаемого изображения.
 * @param {String} soundUrl Url path to sound. Путь к загружаемому звуку.
 * @returns {void}
 */
TrinAssetLoader.prototype.addSound = function(soundName, soundUrl, allowMP3, looped) {
    if (TrinGame.debugger.debug)    {
        soundUrl += "?rnd=" + Math.random();
    }
    if (allowMP3 === undefined) {
        allowMP3 = false;   
    }
	if (looped === undefined)	{
		looped = false;
	}
    this.assetsToLoad.push({
        name: soundName,
        src: soundUrl,
        type: this.TYPE_SOUND,
        allowMP3: allowMP3,
		looped: looped
    });
};

/**
 * Add json toloading queue. 
 * Добавляет файл JSON в очередь загрузки.
 * @param {String} jsonName
 * @param {String} jsonUrl
 * @returns {void}
 */
TrinAssetLoader.prototype.addJson = function(jsonName, jsonUrl) {
    if (TrinGame.debugger.debug)    {
        jsonUrl += "?rnd=" + Math.random();
    }
    this.assetsToLoad.push({
        name: jsonName,
        src: jsonUrl,
        type: this.TYPE_JSON
    });
};

/**
 * Start the assetloading. 
 * Начинает загрузку ресурсов.
 * @returns {void}
 */
TrinAssetLoader.prototype.startLoading = function() {
    this.loadNext();
};

/**
 * Start loading of next asset in queue. 
 * Начинает загрузку следующего ресурса в очереди.
 * @returns {void}
 */
TrinAssetLoader.prototype.loadNext = function() {
    if (this.currentAsset === this.assetsToLoad.length) {
        this.process(1);
        this.loadingEnded();
        return;
    }
    this.currentProgress = this.currentAsset / this.assetsToLoad.length;
    this.process(this.currentProgress);
    var info = this.assetsToLoad[this.currentAsset++];
    switch (info.type) {
        case this.TYPE_IMAGE:
            var xobj = new XMLHttpRequest();
            var ext = info.src.substring(info.src.lastIndexOf(".") + 1, info.src.length);
            if (xobj.overrideMimeType){
                xobj.overrideMimeType("image/" + ext);
            }
            xobj.open('GET', info.src, true);
            xobj.onreadystatechange = function() {
                if (xobj.readyState === 4) {
                    var info = arguments.callee.info;
                    if (xobj.status === 200 || xobj.status === 0)
                    {
                        var obj = new Image();
                        obj.info = info;
						TrinAnimation.prototype.addImage(obj);
						obj.onload = function(){
							if (!arguments.callee.obj.info.animated)	{
								TrinAnimation.prototype.makeAnimation(arguments.callee.obj.info.name, arguments.callee.obj.info.name);
							}
							arguments.callee.loader.loadNext();
						}
						obj.onload.loader =  arguments.callee.loader;
						obj.onload.obj =  obj;
                        obj.src = info.src;
						obj.onerror = obj.onload;
                        xobj.onreadystatechange = null;
                    }   else    {
                        TrinGame.debugger.log("AssetLoaderFileNotFound", info.src);
                    }
                }
            };
            xobj.onreadystatechange.info = info;
            xobj.onreadystatechange.loader = this;
            xobj.onprogress = function(event) {
                var l = arguments.callee.loader;
                l.process(l.currentProgress + (1 / l.assetsToLoad.length) * (event.loaded/event.total));            
            };
            xobj.onprogress.loader = this;
            xobj.send(null);
            break;
        case this.TYPE_SOUND:
			var sound;
			if (Howl === undefined)	{
				var sound = new Audio();
				var src;
				src = document.createElement("source");
				src.setAttribute("src", info.src + ".aac");
				src.setAttribute("type", "audio/aac");
				sound.appendChild(src);

				src = document.createElement("source");
				src.setAttribute("src", info.src + ".ogg");
				src.setAttribute("type", "audio/ogg");
				sound.appendChild(src);

                src = document.createElement("source");
                src.setAttribute("src", info.src + ".mp3");
                src.setAttribute("type", "audio/mpeg");
                sound.appendChild(src);
                
				sound.info = info;
				sound.loader = this;
				sound.addEventListener("canplaythrough", TrinAssetLoader.prototype.onAudioLoad, false);
				sound.addEventListener("progress", TrinAssetLoader.prototype.onAudioProgress, false);
				sound.addEventListener("error", TrinAssetLoader.prototype.onAudioError, false);
				sound.volume = 0;
				sound.play();	
				TrinSound.prototype.add(sound);		
				if (TrinGame.device === TrinUtil.prototype.DEVICE_ANDROID && TrinGame.browser === TrinUtil.prototype.BROWSER_CHROME)    {
					this.loadNext();   
				}
			}	else	{
				sound = new Howl({
					urls: [info.src + ".aac", info.src + ".ogg", info.src + ".mp3"],
					autoplay: false,
					loop: info.looped
				});
				sound.info = info;
				sound.load();	
				TrinSound.prototype.add(sound);	
				this.loadNext();   
			}
	        break;
        case this.TYPE_JSON:
            var xobj = new XMLHttpRequest();
            if (xobj.overrideMimeType){
                xobj.overrideMimeType("application/json");
            }
            xobj.open('GET', info.src, true);
            xobj.onreadystatechange = function() {
                if (xobj.readyState === 4) {
                    var info = arguments.callee.info;
                    if (xobj.status === 200 || xobj.status === 0)
                    {
						try {
							var obj = JSON.parse(xobj.responseText);
							TrinUtil.prototype.LOADED_JSON[info.name] = obj;
							xobj.onreadystatechange = null;
							arguments.callee.loader.loadNext();
						}catch(err){
							arguments.callee.loader.loadNext();
						}
                    } else if (xobj.status === 404)      {
                        TrinGame.debugger.log("AssetLoaderFileNotFound", info.src);
                        arguments.callee.loader.loadNext();						
                    }
                }
            };
            xobj.onreadystatechange.loader = this;
            xobj.onreadystatechange.info = info;
            xobj.send(null);
            break;
    }
};

TrinAssetLoader.prototype.onAudioProgress = function(event){var propValue;
    var l = this.loader;
	this.pause();
    if (l === undefined)    {
        return;   
    }
    l.process(l.currentProgress + (1 / l.assetsToLoad.length) * 0.5); 
};

TrinAssetLoader.prototype.onAudioLoad = function(){
    //alert("Loaded: " + this.info.src);
	this.pause();
    this.volume = 1;
	this.removeEventListener("canplaythrough", TrinAssetLoader.prototype.onAudioLoad, false);
	this.removeEventListener("progress",  TrinAssetLoader.prototype.onAudioProgress, false);
    this.removeEventListener("error", TrinAssetLoader.prototype.onAudioError, false);
    if (TrinGame.device !== TrinUtil.prototype.DEVICE_ANDROID || TrinGame.browser !== TrinUtil.prototype.BROWSER_CHROME)    {
	   this.loader.loadNext();
    }
	this.loader = undefined;
};

TrinAssetLoader.prototype.onAudioError = function(e){
    //alert("Failed: " + this.info.src);
	this.removeEventListener("canplaythrough", TrinAssetLoader.prototype.onAudioLoad, false);
	this.removeEventListener("progress",  TrinAssetLoader.prototype.onAudioProgress, false);
    this.removeEventListener("error", TrinAssetLoader.prototype.onAudioError, false);
	this.loader.loadNext();
	this.loader = undefined;
};

/**
 * Loading complete. 
 * Завершение загрузки.
 * @returns {void}
 */
TrinAssetLoader.prototype.loadingEnded = function() {
    this.currentAsset = 0;
    this.assetsToLoad = [];
    if (this.onComplete !== null) {
        this.onComplete();
    }
};

/**
 * Loading process. 
 * Процесс загрузки.
 * @param {Number} percent Percent of loading from 0 to 1. Процент загрузки от 0 до 1.
 * @returns {void}
 */
TrinAssetLoader.prototype.process = function(percent) {
    if (this.onProgress !== null) {
        this.onProgress(percent);
    }
};